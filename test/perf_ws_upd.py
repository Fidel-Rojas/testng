import logging
import pytest
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
#from selenium.webdriver.support.ui import WebDriverWait
#from selenium.webdriver.support import expected_conditions as EC
#from selenium.webdriver.common.action_chains import ActionChains
import random
import code
import base_case
from fsm_gui_data_packets import TechStatus
import fake
import tech_event_data
import datetime

log = logging.getLogger(__name__)

null = None
class TC(base_case.BaseCaseFmTab):
   
   def my_upd_job(self, i):
      job = fake.get_job(i)
      t = random.randint(0, 9)
      self.assign_job(job, self.create_tech(t), est=random.choice([8, 9, 10, 11, 12, 13, 14, 15, 16, 17]))
      job.entity_timestamp = self.backend_emu.utcnow().isoformat()
      h=random.randint(6, 15)
      today = str(datetime.date.today())
      job.duration_mins = random.choice([60, 30, 20, 90, 45])
      job.est = f"{today} {str(h).zfill(2)}:00:00-05:00"


      return ['job', job.data]

   def my_upd_tech_status(self, i):
      #tech = fake.get_tech(1000-i)
      tech = self.techs[i % 30]
      tech.tech_status_cd = random.choice(['LON', 'LOFF', 'ENROUTE', 'WRAPUP', 'AV'])
      tech.tech_status_datetime = self.backend_emu.utcnow().isoformat()
      tech_status = TechStatus.from_obj(tech)
      return ['tech_status', tech_status.data]
   
   def my_upd_tech(self, i):
      tech = self.techs[i % 30]
      tshs = tech.get_tshs()
      tsh = tshs[0]
      bt = random.choice([6,7,8,9,10,11,12])
      tsh.shift_begin_time = datetime.time(bt).isoformat('minutes'),
      return ['tech', tech.data]

   def test_websocket_perf(self):
      self.emu.set_response("/jobs", fake.jobs(6000))
      self.emu.set_response("/techs", fake.techs(1800))
      self.emu.set_response({"operationName":"getTechEvents"},
                               tech_event_data.get([fake.get_tech(i).tech_id for i in range(30)]))
      self.techs = [fake.get_tech(i) for i in range(30)]
      freq = dict(start_after_seconds=10, num_seconds=60, num_updates_per_second=5)
      self.emu.setup_ws_updates(label='tech_status', **freq, get_ws_upd_fn=self.my_upd_tech_status)
      self.emu.setup_ws_updates(label='tech', **freq, get_ws_upd_fn=self.my_upd_tech)
      self.emu.setup_ws_updates(label='job', **freq, get_ws_upd_fn=self.my_upd_job)
      self.emu.setup_psinfo(start_after_seconds=20, num_seconds=50)

      self.click_to_flctr3900()
      
      self.emu.await_updates_completed()
      code.interact(local=locals())

