import logging
import code
import base_case
log = logging.getLogger(__name__)

class TestBasic(base_case.BaseCaseFmTab):

   def test_job_tile_basic(self):
      tech0 = self.add_tech(0)
      job0 = self.add_job(0)
      job1 = self.add_job(1, tech=0)
      self.click_to_flctr3900()
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))

   def test_job_tile_job_labels_and_events(self):
      tech0 = self.add_tech(0)
      job0 = self.add_job(0, tech=0, tags=['VIP'], event_types=['NM'])
      job1 = self.add_job(1, tech=0, tags=['VIP', 'WOW'], event_types=['NM', 'META', 'NOLATLONG'])
      self.click_to_flctr3900()
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))

class TestResponsiveToWsUpd(base_case.BaseCaseFmTab):

   def test_job_tile(self):
      tech0 = self.add_tech(0)
      job0 = self.add_job(0) 
      job1 = self.add_job(1)
      self.click_to_flctr3900()
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))
      self.assign_job(job1, tech0)
      self.ws_upd_job(job1)
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))
   
   def test_search_by_job_num(self):
      tech0 = self.add_tech(0)
      job0 = self.add_job(0)
      job1 = self.add_job(1)
      self.click_to_flctr3900()
      self.search_job(job1.job_number)
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job1))
      self.search_job("")
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))
      job2 = self.create_job(2)
      self.search_job(job2.job_number)
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp())
      self.emu.send_job_update(job2)
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job2))

   def test_sort_by_status(self):
      tech0 = self.add_tech(0)
      job0 = self.add_job(0)
      job1 = self.add_job(1)
      self.click_to_flctr3900()
      self.job_list_organize('Sort', 'Status')
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))
      self.assign_job(job1, tech0)
      self.ws_upd_job(job1)
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job1, job0))
      job2 = self.create_job(2)
      self.complete_job(job2)
      self.ws_upd_job(job2)
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job1, job2, job0))
      self.in_service_job(job1)
      self.ws_upd_job(job1)
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job2, job1, job0))

   def test_group_by_status(self):
      tech0 = self.add_tech(0)
      job0 = self.add_job(0)
      job1 = self.add_job(1)
      self.click_to_flctr3900()
      self.job_list_organize('Group', 'Status')
      self.assert_content(lambda:self.get_job_list_counts(), {'Unassigned':'2 Jobs'})
      job2 = self.create_job(2)
      self.complete_job(job2)
      self.ws_upd_job(job2)
      self.assign_job(job1, tech0)
      self.ws_upd_job(job1)
      self.assert_content(lambda:self.get_job_list_counts(), {'Unassigned':'1 Jobs', 'Assigned':'1 Jobs', 'Complete':'1 Jobs'})




