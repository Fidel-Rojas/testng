import logging
import code
import time
from datetime import datetime
from unittest.mock import ANY
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import base_case
import fake
from fsm_gui_data_packets import SupportTask

log = logging.getLogger(__name__)
null = None

class SqmBase(base_case.BaseCaseSqmTab):

   def get_dispositions(self):
      return [39000050]
   
   def get_dispositions_config(self):
      log.info("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGg")
      return {
         "isRequired": True,
         "dispositionDefinitions": [
            {"id": 39000050, "name": "dispo-0-50", "parentId": null},
            {"id": 39000051, "name": "dispo-0-51", "parentId": null},
            {"id": 39000052, "name": "dispo-0-52", "parentId": null}
         ]}

   def get_resolutions(self):
      return [
         dict(
            taskId=39000000,
            resolutionId=101,
            resolutionText='resolution 1'),
         dict(
            taskId=39000000,
            resolutionId=102,
            resolutionText='resolution 2'),
      ]
      
   def get_subjects(self):
      return dict(List=[
         {"SupportTaskDefnId": 39000000,
          "Subject": "taskdefn-0-0",
          "Message": "taskdefn0 message",
          "DisplayOrder": 1,
          "SupportTaskCategoryDefnId": 39000000,
          "Category": "cat-0-0"},
         {"SupportTaskDefnId": 39000001,
          "Subject": "taskdefn-0-1",
          "Message": "taskdefn1 message",
          "DisplayOrder": 1,
          "SupportTaskCategoryDefnId": 39000001,
          "Category": "cat-0-1"},
      ])
      
   def get_messages(self):
      return [
         {"taskId":39000000,
          "messageId":null,
          "personnelId":39099999,
          "enterpriseFirstName":"David",
          "enterpriseLastName":"Totschnig",
          "techId":39000000,
          "submitDate":"2023/05/15 04:00:00.000",
          "readDate":"2023/05/15 13:24:33.920",
          "createdBy":"UNKNOWN",
          "message":"support task message here"
          },
         {"taskId":39000000,
          "messageId":469,
          "personnelId":39099999,
          "enterpriseFirstName":"David",
          "enterpriseLastName":"Totschnig",
          "techId":39000000,
          "submitDate":"2023/05/15 12:33:01.972",
          "readDate":null,
          "createdBy":"AGENT",
          "message":
          "support task message here David Totschnig has accepted this task."}]
   
   def get_techmessage_readdate(self):
      return {"taskId":39000000}

   def get_msgtasks(self):
      return {
         "Subject": "taskdefn-0-1",
         "Id": 39000001,
         "Type": "SUPPORT_TASK",
         "Sender": {
            "Type": "TECH",
            "Id": "T000",
            "IdType": "WFX_IDENTIFIER"
         },
         "MessageId": "2202",
         "Body": "taskdefn1 message",
         "FulfillmentCenter": 3900,}
      
   def get_graphql_get_techs_by_tech_ids(self):
      graphql_rows = []
      for tech in self.techs:
         graphql_rows.append({
            "techId":tech.tech_id,"techNum":tech.tech_num,
            "firstName":tech.tech_first_name,"lastName":tech.tech_last_name,"__typename":"Tech",
            "techPhoto":null,"status":tech.tech_status_cd,"fulfillmentCenterId":3900})
      return {"data":{"getTechsByTechIds":graphql_rows}}
   
   def get_graphql_get_stask_attachment(self):
      return {"data":{"result":{"attachments":[],"__typename":"SupportTask"}}}

   def get_graphql_tasks_resolution_codes(self):
      return {"data": {
         "defns": [
            {"definitionId": 39000000, "resolutions": [{"id": 39000000,"description": "Equipment resolved","__typename": "SupportTaskResolution"}],
             "__typename": "SupportTaskDefinitionResolution"
             },
            # {"definitionId": 39000001, "resolutions": [{"id": 39000001,"description": "Provisioned","__typename": "SupportTaskResolution"},
            #                                            {"id": 39000002,"description": "Cancel","__typename": "SupportTaskResolution"},],
            #  "__typename": "SupportTaskDefinitionResolution"
            #  },
         ]}}
         # {'definitionId': 643, 'resolutions':[{'id': 241, 'description': 'Complete', "__typename":"ResolutionCode"}], "__typename":"SupporTaskDefinition"},
         # {'definitionId': 644, 'resolutions':[{'id': 184, 'description': 'Equipment resolved', "__typename":"ResolutionCode"}], "__typename":"SupporTaskDefinition"},
         # {'definitionId': 645, 'resolutions':[{'id': 184, 'description': 'Equipment resolved', "__typename":"ResolutionCode"}], "__typename":"SupporTaskDefinition"},
         # {'definitionId': 646, 'resolutions':[{'id': 182, 'description': 'Provisioned', "__typename":"ResolutionCode"},
         #                                      {'id': 243, 'description': 'Cancel', "__typename":"ResolutionCode"}], "__typename":"SupporTaskDefinition"}
   
   def get_wexis_msgtasks_task_accept(self, st):
      log.info("in get_wexis_msgtasks_task_accept")
      self.task_accept_called = True
      st = self.to_support_task(st)
      return {
         "mso":"mso3930",
         "msoBuId":3930,
         "personnelId":st.ent_prsn_id,
         "supportTaskId":st.st_id,
         "fulfillmentCenter":st.flctr_bu_id,
         "techId":st.tech_id,
         "definitionId":st.std_id,
         "subject":st.subject,
         "message":st.body,
         "category":st.category,
         "workOrderNumber":st.work_order_number,
         "createDate":st.create_dt,
         "priority":2}
   
class TestSidebarClickForTaskDetails(SqmBase):

   def test_it(self):
      self.emu.set_response({'path': '1.0/dispositions/39000000'}, self.get_dispositions)
      self.emu.set_response({'path': '1.0/msgtasks/39000000/resolutions'}, self.get_resolutions)
      self.emu.set_response({'path': '1.0/workorders/3900/subjects'}, self.get_subjects)
      self.emu.set_response({'path': '1.0/msgtasks/39000000/messages'}, self.get_messages)
      self.emu.set_response({'operationName': "GetTechsByTechIds"}, self.get_graphql_get_techs_by_tech_ids)
      self.emu.set_response({'operationName': 'GetSTaskAttachment'}, self.get_graphql_get_stask_attachment)
      tech0 = self.add_tech(0)
      job0 = self.add_job(0)
      job1 = self.add_job(1, tech=0)
      self.add_support_task(0, ent_prsn_id=self.get_user_id(), status='WORKING', accept_dt=self.utcnow())
      self.add_support_task(1, ent_prsn_id=self.get_user_id(), status='WORKING', accept_dt=self.utcnow())
      self.add_support_task(2, status='INQUEUE')
      self.add_support_task(3, status='INQUEUE')
      self.navigate_to_sqm()
      self.click_sidebar_support_tasks_button()
      self.click_sidebar_support_task_item_blue_arrow()
      bad_exp_due_to_bad_scrape = {'status': 'WORKING', 'time': ANY, 'task_id': '39000000', 'category': 'Needs Backup', 'task_defn_name': 'Please Help', 'create_dt': '2023 January 1, 12:00 PM', 'assigned_initials': 'SY', 'assigned_name': 'T000 Susan Young', 'associated_initials': 'SY', 'associated_status': 'WORKING', 'associated_name': 'T000 Susan Young'}
      self.assert_content(lambda:self.get_sidebar_support_task_selected_container(), bad_exp_due_to_bad_scrape)
      # need to add messages to get_sidebar_support_task_selected_container
      self.click_sidebar_all_tasks()
      self.assert_content(lambda:self.get_sidebar_support_task_list(),
                          {'MY TASKS': '2', 'IN QUEUE': '2', 'support_tasks':self.sidebar_support_tasks_exp(0, 1)})
      
class TestSqmClickForTaskDetails(SqmBase):

   def test_it(self):
      self.emu.set_response({'path': '1.0/dispositions/39000000'}, self.get_dispositions)
      self.emu.set_response({'path': '1.0/msgtasks/39000000/resolutions'}, self.get_resolutions)
      self.emu.set_response({'path': '1.0/workorders/3900/subjects'}, self.get_subjects)
      self.emu.set_response({'path': '1.0/msgtasks/39000000/messages'}, self.get_messages)
      self.emu.set_response({'path': '1.0/msgtasks/39000000/techmessages/readdate'}, self.get_techmessage_readdate)
      self.emu.set_response({'operationName': "GetTechsByTechIds"}, self.get_graphql_get_techs_by_tech_ids)
      self.emu.set_response({'operationName': 'GetSTaskAttachment'}, self.get_graphql_get_stask_attachment)
      self.add_support_task(0, ent_prsn_id=self.get_user_id(), status='WORKING', accept_dt=self.utcnow())
      self.navigate_to_sqm()
      self.double_click(self.xpath_by_class('tile'))
      bad_exp_due_to_bad_scrape = {'status': 'WORKING', 'time': ANY, 'task_id': '39000000', 'category': 'Needs Backup', 'task_defn_name': 'Please Help', 'create_dt': 'Please Help', 'assigned_initials': 'DT', 'assigned_name': 'David Totschnig', 'associated_initials': 'DT', 'associated_name': 'David Totschnig', 'msg_info': {'badge': None, 'messages': []}}
      self.assert_content(lambda:self.get_sqm_support_task_details_container(), bad_exp_due_to_bad_scrape)
      self.click(self.xpath_by_class('expand-circle'))
      bad_exp_due_to_bad_scrape['msg_info'] = dict(
         badge = 1,
         messages = [
            {'message': 'support task message here', 'sender': '', 'time': 'May 14, 22:00 PM'},
            {'message': 'support task message here David Totschnig has accepted this task.', 'sender': 'D. Totschnig', 'time': 'May 15, 06:33 AM'}])
      self.assert_content(lambda:self.get_sqm_support_task_details_container(), bad_exp_due_to_bad_scrape)
      self.click(self.xpath_by_class('closer'))
      
class TestSqmTaskTileMenu(SqmBase):

   def test_create_support_task(self):
      self.emu.set_response({'path': '1.0/workorders/3900/subjects'}, self.get_subjects)
      self.emu.set_response({'path': "1.1/msgtasks"}, self.get_msgtasks)
      self.emu.set_response({'operationName': "GetTechsByTechIds"}, self.get_graphql_get_techs_by_tech_ids)
      tech0 = self.add_tech(0)
      self.add_support_task(0, ent_prsn_id=self.get_user_id(), status='WORKING', accept_dt=self.utcnow(), tech=0)
      self.navigate_to_sqm()
      tile = self.get(".//task-tile")
      ac = ActionChains(self.driver)
      ac.move_to_element(tile).perform()
      self.find_by_class(self.find_by_class(tile, 'ellipsis'), 'icon').click()
      self.click(f".//*[contains(text(),'Create Support Task')]")
      self.type(".//csg-textarea//textarea", "yoyoyo a little support here eh", Keys.RETURN)
      self.click(self.xpath_by_class('down-arrow'))
      self.click(".//wfx-dropdown//" + self.xpath_by_class("option"))
      self.click('.//*[contains(text(),"Save")]')

   def test_resolve_support_task(self):
      self.emu.set_response({'path': '1.0/workorders/3900/subjects'}, self.get_subjects)
      self.emu.set_response({'path': "1.1/msgtasks"}, self.get_msgtasks)
      self.emu.set_response({'path': '1.0/dispositions/config'}, self.get_dispositions_config) # CANNOT GET THISE TO SHOW UP IN DROP-DOWN even though they do in twebui
      self.emu.set_response({'path': '1.0/dispositions/39000000'}, self.get_dispositions)
      self.emu.set_response({'operationName': "GetTechsByTechIds"}, self.get_graphql_get_techs_by_tech_ids)
      self.emu.set_response({'operationName': 'TasksResolutionCodes'}, self.get_graphql_tasks_resolution_codes)
      self.emu.set_response({'operationName': 'CompleteTask'}, {"data": {"task": "OK"}})
      tech0 = self.add_tech(0)
      self.add_support_task(0, ent_prsn_id=self.get_user_id(), status='WORKING', accept_dt=self.utcnow(), tech=0)
      self.navigate_to_sqm()
      tile = self.get(".//task-tile")
      ac = ActionChains(self.driver)
      ac.move_to_element(tile).perform()
      self.find_by_class(self.find_by_class(tile, 'ellipsis'), 'icon').click()
      self.click(".//*[contains(text(),'Resolve')]")
      self.click('.//support-task-sidebar//wfx-checkbox')
      self.click('.//support-task-sidebar//button[@id="completeTask"]')
      
   def test_unassign_support_task(self):
      self.emu.set_response({'path': '1.0/workorders/3900/subjects'}, self.get_subjects)
      self.emu.set_response({'operationName': 'UnAssignTask'}, {"data": {"task": "OK"}})
      tech0 = self.add_tech(0)
      self.add_support_task(0, ent_prsn_id=self.get_user_id(), status='WORKING', accept_dt=self.utcnow(), tech=0)
      self.navigate_to_sqm()
      tile = self.get(".//task-tile")
      ac = ActionChains(self.driver)
      ac.move_to_element(tile).perform()
      self.find_by_class(self.find_by_class(tile, 'ellipsis'), 'icon').click()
      self.click(".//*[contains(text(),'Unassign')]")
      self.click('.//support-task-sidebar//button')

class TestFiltering(SqmBase):

   def test_it(self):
      self.emu.set_response({'operationName': "GetTechsByTechIds"}, self.get_graphql_get_techs_by_tech_ids)
      tech0 = self.add_tech(0)
      job0 = self.add_job(0)
      job1 = self.add_job(1, tech=0)
      defaults = dict(ent_prsn_id=self.get_user_id(), accept_dt=self.utcnow())
      self.add_support_task(0, **defaults, category='AAA', new_message_cnt=0, status='WORKING')
      self.add_support_task(1, **defaults, category='BBB', new_message_cnt=0, status='WORKING')
      self.add_support_task(2, **defaults, category='AAA', new_message_cnt=1, status='WORKING')
      self.add_support_task(3, **defaults, category='BBB', new_message_cnt=1, status='WORKING')
      self.add_support_task(4, **defaults, category='AAA', new_message_cnt=0, status='COMPLETED')
      self.add_support_task(5, **defaults, category='BBB', new_message_cnt=0, status='COMPLETED')
      self.add_support_task(6, **defaults, category='AAA', new_message_cnt=1, status='COMPLETED')
      #self.add_support_task(7, **defaults, category='BBB', new_message_cnt=1, status='COMPLETED')
      self.navigate_to_sqm()
      self.click_sidebar_filter_button()
      self.click_sqm_filter_tab('Category')
      self.click_sqm_filter_tab_entry('BBB')
      sts = self.support_tasks
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(sts[1], sts[3], sts[5]))
      self.click_sqm_filter_tab('Status')
      self.click_sqm_filter_tab_entry('COMPLETED')
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(sts[5]))
      self.click_sqm_filter_tab('Messages')
      self.click_sqm_filter_tab_entry('Unread')
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp())
      self.click_sqm_filter_tab_entry('Unread')
      self.click_sqm_filter_tab_entry('Read')
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(sts[5]))
      self.click_sqm_filter_tab_entry('Read')
      self.click_sqm_filter_tab('Category')
      self.click_sqm_filter_tab_entry('AAA')
      self.click_sqm_filter_tab_entry('BBB')
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(sts[4], sts[6]))
      self.click_sqm_filter_tab('Status')
      self.click_sqm_filter_tab_entry('COMPLETED')
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(sts[0], sts[2], sts[4], sts[6]))
      

class TestSortBy(SqmBase):

   def test_it(self):
      self.emu.set_response({'operationName': "GetTechsByTechIds"}, self.get_graphql_get_techs_by_tech_ids)
      tech0 = self.add_tech(0)
      job0 = self.add_job(0)
      job1 = self.add_job(1, tech=0)
      defaults = dict(ent_prsn_id=self.get_user_id(), category="AAA")
      self.add_support_task(0, **defaults, subject="Red",    status='WORKING',   accept_dt=datetime(2023,1,1,12), create_dt=datetime(2023,1,1,8))
      self.add_support_task(1, **defaults, subject="Yellow", status='WORKING',   accept_dt=datetime(2023,1,1,5),  create_dt=datetime(2023,1,1,7))
      self.add_support_task(2, **defaults, subject="Blue",   status='WORKING',   accept_dt=datetime(2023,1,1,9),  create_dt=datetime(2023,1,1,6))
      self.add_support_task(3, **defaults, subject="Purple", status='COMPLETED', accept_dt=datetime(2023,1,1,7),  create_dt=datetime(2023,1,1,4))
      self.add_support_task(4, **defaults, subject="Green",  status='COMPLETED', accept_dt=datetime(2023,1,1,14), create_dt=datetime(2023,1,1,5))
      self.add_support_task(5, **defaults, subject="Orange", status='COMPLETED', accept_dt=datetime(2023,1,1,10), create_dt=datetime(2023,1,1,9))
      self.navigate_to_sqm()
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(0, 1, 2, 3, 4, 5))
      self.click_sqm_sort_by('Subject')
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(2, 4, 5, 3, 0, 1))
      self.click_sqm_sort_by('Accepted Time')
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(4, 0, 5, 2, 3, 1))
      self.click_sqm_sort_by('Creation Time')
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(5, 0, 1, 2, 4, 3))
      self.click_sqm_sort_by('Status')
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(0, 1, 2, 3, 4, 5))
      
class TestGroupBy(SqmBase):

   def test_it(self):
      self.emu.set_response({'operationName': "GetTechsByTechIds"}, self.get_graphql_get_techs_by_tech_ids)
      tech0 = self.add_tech(0)
      tech0 = self.add_tech(1)
      mine = dict(ent_prsn_id=self.get_user_id())
      defaults = dict()
      self.add_support_task(0, **defaults, **mine, category="AAA", status='WORKING',   new_message_cnt=0, tech=0)
      self.add_support_task(1, **defaults, **mine, category="AAA", status='WORKING',   new_message_cnt=1)
      self.add_support_task(2, **defaults,         category="BBB", status='WORKING',   new_message_cnt=9, tech=0)
      self.add_support_task(3, **defaults,         category="BBB", status='COMPLETED', new_message_cnt=0, tech=1)
      self.add_support_task(4, **defaults,         category="BBB", status='COMPLETED', new_message_cnt=0, tech=0)
      self.add_support_task(5, **defaults,         category="CCC", status='COMPLETED', new_message_cnt=9, tech=0)
      self.navigate_to_sqm()
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(0, 1, 2, 3, 4, 5))
      self.click_sqm_group_by('Agent')
      self.assert_content(lambda:self.get_sqm_group_counts(), {'No Agent Specified': '4 Support Tasks', 'Totschnig, David': '2 Support Tasks'})
      self.click_sqm_group_by('Category')
      self.assert_content(lambda:self.get_sqm_group_counts(), {'AAA': '2 Support Tasks', 'BBB': '3 Support Tasks', 'CCC': '1 Support Tasks'})
      self.click_sqm_group_by('Messages')
      self.assert_content(lambda:self.get_sqm_group_counts(), {'Read': '3 Support Tasks', 'Unread': '3 Support Tasks'})
      self.click_sqm_group_by('Status')
      self.assert_content(lambda:self.get_sqm_group_counts(), {'COMPLETED': '3 Support Tasks', 'WORKING': '3 Support Tasks'})
      self.click_sqm_group_by('Technician')
      self.assert_content(lambda:self.get_sqm_group_counts(), {'No Supervisor Specified': '1 Support Tasks', self.tech_last_first(1): '1 Support Tasks', self.tech_last_first(0): '4 Support Tasks'})
      self.click_sqm_group_by('Ungroup')
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(0, 1, 2, 3, 4, 5))

   def tech_last_first(self, t):
      tech = self.techs[t]
      return f"{tech.tech_last_name}, {tech.tech_first_name}"
      
class TestSearch(SqmBase):

   def test_it(self):
      self.emu.set_response({'operationName': "GetTechsByTechIds"}, self.get_graphql_get_techs_by_tech_ids)
      tech0 = self.add_tech(0)
      self.add_tech(1)
      mine = dict(ent_prsn_id=self.get_user_id())
      defaults = dict()
      self.add_support_task(0, **defaults, **mine, category="AAA", status='WORKING',   subject="Ant",    tech=0)
      self.add_support_task(1, **defaults, **mine, category="ANT", status='WORKING',   subject="Roach",  tech=None)
      self.add_support_task(2, **defaults,         category="BBB", status='WORKING',   subject="Mantis", tech=0)
      self.add_support_task(3, **defaults,         category="BBB", status='COMPLETED', subject="Beetle", tech=1)
      self.add_support_task(4, **defaults,         category="EEE", status='COMPLETED', subject="Sartre", tech=0)
      self.add_support_task(5, **defaults,         category="MAN", status='COMPLETED', subject="Locke",  tech=0)
      self.navigate_to_sqm()
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(0, 1, 2, 3, 4, 5))
      self.type(".//input[@placeholder='Search Support Tasks']", "ant")
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(0, 1, 2))
      self.type(".//input[@placeholder='Search Support Tasks']", "ANT")
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(0, 1, 2))
      self.type(".//input[@placeholder='Search Support Tasks']", "Ant")
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(0, 1, 2))
      self.type(".//input[@placeholder='Search Support Tasks']", "EE")
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(3, 4))
      self.type(".//input[@placeholder='Search Support Tasks']", tech0.tech_last_name)
      self.assert_content(lambda:self.get_sqm_support_tasks(), self.sqm_support_tasks_exp(0, 2, 4, 5))

class TestSidebarAcceptTask(SqmBase):

   def test_it(self):
      self.emu.set_response({'path': '1.0/msgtasks/39000000/messages'}, self.get_messages)
      self.emu.set_response({'path': '1.0/msgtasks/task/accept'}, lambda:self.get_wexis_msgtasks_task_accept(2))
      self.emu.set_response({'operationName': "GetTechsByTechIds"}, self.get_graphql_get_techs_by_tech_ids)
      tech0 = self.add_tech(0)
      job0 = self.add_job(0)
      job1 = self.add_job(1, tech=0)
      self.add_support_task(0, ent_prsn_id=self.get_user_id(), status='WORKING', accept_dt=self.utcnow())
      self.add_support_task(1, ent_prsn_id=self.get_user_id(), status='WORKING', accept_dt=self.utcnow())
      self.add_support_task(2, status='INQUEUE')
      self.add_support_task(3, status='INQUEUE')
      self.navigate_to_sqm()
      self.click_sidebar_support_tasks_button()
      self.assert_content(lambda:self.get_sidebar_support_task_list(),
                          {'MY TASKS': '2', 'IN QUEUE': '2', 'support_tasks':self.sidebar_support_tasks_exp(0, 1)})
      self.my_click_accept_task()
      self.assert_content(lambda:self.get_sidebar_support_task_list(),
                          {'MY TASKS': '3', 'IN QUEUE': '1', 'support_tasks':self.sidebar_support_tasks_exp(0, 1, 2)})
      self.my_other_user_support_task_update()
      self.assert_content(lambda:self.get_sidebar_support_task_list(),
                          {'MY TASKS': '3', 'IN QUEUE': '0', 'support_tasks':self.sidebar_support_tasks_exp(0, 1, 2)})

   def my_click_accept_task(self):
      self.task_accept_called = False
      #code.interact(local=locals())
      self.click_sidebar_accept_task()
      for i in range(10):
         if self.task_accept_called:
            break
         log.info("check %s", self.task_accept_called)
         time.sleep(0.1)
      assert self.task_accept_called
      st = self.support_tasks[2]
      st.status = 'WORKING'
      st.subject = 'New Subject'
      st.accept_dt = self.utcnow()
      st.ent_prsn_id = self.get_user_id()
      self.ws_upd_support_task(st)
      
   def my_other_user_support_task_update(self):
      st3 = self.support_tasks[3]
      st3.status = 'WORKING'
      st3.accept_dt = self.utcnow()
      st3.ent_prsn_id = 987654321
      self.ws_upd_support_task(st3)

      #code.interact(local=locals())
"""
todo
----
DONE sidebar click to display task 1
DONE sqm click to display task 2
DONE create task from sqm display task tile menu
DONE resolve from sqm display task tile menu
DONE unassign from sqm display task tile menu
DONE filtering categories etc
DONE sort by
DONE group by
DONE search
new support_task_comm -> chat icon appears in support-task-tile (broken)
load test
  specs:
    100,000 support tasks (support tasks loaded per ent_prsn so no no)
    10 categories
    50 tasks per ent_prsn_id -> 2000 ent_prsns
    30k techs for 100k tasks
    50% of tasks have wo -> 50k wos
    over 10 hour window 500,000 comms
  test filtering, sortBy, groupBy, search
DONE rtu support task updates
rtu to update counts
rtu filtering
rtu sort
rtu group
rtu search
"""
