import logging
import pytest
import asyncio
import aiohttp
import re
import os
import ssl
import time
import pickle
import requests
import traceback
import json
import urllib.parse
import requests
import types
import inspect
from types import SimpleNamespace as DotDict
import funcy
import code
from selenium import webdriver
from selenium.webdriver.common.by import By
from seleniumbase import config as sb_config
#from graphql_ws.aiohttp import AiohttpSubscriptionServer
#import my_graphql_schema
import fake
import emu_responses
# from ..conftest import Util

pytest.register_assert_rewrite("base_case")

def pytest_addoption(parser):
   parser.addoption(
      '--code-interact', action='store_true', help='drop into python interpreter on assertion failure')
   parser.addoption(
      '--code-interact-after-test', action='store_true', help='drop into python interpreter after test')

# subscription_server = AiohttpSubscriptionServer(my_graphql_schema.schema)
async def subscriptions(request):
   pass
#    ws = web.WebSocketResponse(protocols=('graphql-ws',))
#    await ws.prepare(request)
#    log.info('GGG')
#    await subscription_server.handle(ws)
#    log.info('GGG')
#    return ws

# @pytest.fixture()
# def driver():
#    options = webdriver.ChromeOptions()
#    options.add_argument('ignore-certificate-errors')
#    return webdriver.Chrome(options=options)


import sys
import os
import asyncio
import aiohttp
from aiohttp import web
from datetime import datetime, date
import logging
sys.path.insert(1, './pipmodules')
import pytz
import pytest
import pytest_asyncio
import functools
import socket

log = logging.getLogger('conftest')

############## helper funcs ##############

# @pytest.fixture(scope="module")
# def event_loop():
#     loop = asyncio.get_event_loop()
#     yield loop
#     loop.close()

############## end scope module ##############

      
global_backend_emu = None
global_twebui_forward = os.environ.get('TWEBUI_FORWARD') == 'ON'
global_capture = os.environ.get('CAPTURE') == 'ON'
global_replay = os.environ.get('REPLAY') == 'ON'
global_captures = []
global_replays = []

@pytest.fixture()
def setup(request, backend_emu):
   log.info('fixture setup %s', request.node.name)
   global global_backend_emu
   global_backend_emu = backend_emu
   global global_captures
   global_captures = []
   capture_filename = get_capture_filename(request)
   if global_replay:
      log.info("read %s", capture_filename)
      if os.path.exists(capture_filename):
         with open(capture_filename, 'rb') as f:
            data = pickle.load(f)
         global_replays = data['captures']
         log.info("read %s done %s", capture_filename, len(global_replays))
      else:
         log.info("INFO: capture file not found: %s", capture_filename)
   yield
   #httpserver.check_assertions()
   if global_capture:
      write_captures(capture_filename, global_captures)

@pytest.fixture()
def mso_fixture():
   return get_mso()
      
def get_mso():
   return 'mso3930'

def get_capture_filename(request):
   function = request.node.name.replace('\\', '_').replace('/', '_')
   fspath = str(request.node.fspath)
   module = fspath[fspath.rindex('/')+1:-3]
   name = f"./captures/{module}-{function}.pickle"
   log.info('get_capture_filename %s', name)
   return name

def write_captures(filename, capture_data):
   data = dict(captures=capture_data)
   log.info("write_captures %s %s", filename, len(capture_data))
   #for d in capture_data:
   #log.info("write_captures data %s", d)
   with open(filename, 'wb') as f:
      pickle.dump(data, f, -1)
   log.info("wrote %s", filename)

def json_serialize(obj):
   """JSON serializer for objects not serializable by default json code"""
   if isinstance(obj, (datetime, date)):
      return obj.isoformat()
   raise TypeError(f"Type {type(obj)} not serializable")

def jsonify(obj):
   return json.dumps(obj, separators=(',',':'), default=json_serialize)
   
import urllib.parse
def decode_wexis_req(data):
   if isinstance(data, bytes): #### WHY is the type not always one or the other?
      data = data.decode('utf-8')
   if re.match(r".*wexis", data):
      obj = decode_wexis_body(data)
      target = obj['args'].get('target')
      if target:
         if 'SinceDate' in target['query']:
            target['query'].pop('SinceDate')
      obj = {'rp':obj['rp'], 'target':target}
      data = json.dumps(obj, sort_keys=True)
   return data

def decode_wexis_body(data):
   rp, args = data.split('&', 2)
   rp = rp[3:]
   args = json.loads(urllib.parse.unquote(args[5:]))
   target = args.get('target')
   if target:
      p = urllib.parse.urlparse(target)
      query = urllib.parse.parse_qs(p.query)
      args['target'] = dict(path=p.path, query=query)
   obj = {'rp':rp, 'args':args}
   return obj
      
def decode_body(data):
   if isinstance(data, bytes): #### WHY is the type not always one or the other?
      data = data.decode('utf-8')
   if re.match(r".*wexis", data):
      obj = decode_wexis_body(data)
      target = obj.get('args', {}).get('target', {}) # wexis
      if target:
         obj = {'rp':obj.get('rp'), **target}
   elif data:
      obj = json.loads(data)
   else:
      obj = None
   return obj

async def get_replay_response(request):
   path = request.path
   data = await request.text()
   data = decode_wexis_req(data)
   log.info("REPLAY FIND %s %s", path, data)
   if not global_captures:
      log.info("ERROR NO REPLAY DATA")
      raise Exception("ERROR NO REPLAY DATA, NO RESPONSE SPECIFIED")
   found = None
   for entry in global_captures:
      # Need to be careful accessing the text property, it will not exist for non text-based responses.
      ereq = entry['request']
      #log.info('check-url %s %s', ereq['path'], path)
      if ereq['path'] == path:
         ereq_text = decode_wexis_req(ereq.get('data'))
         if ereq_text == data:
            found = entry
            break
      if found:
         break
   if found:
      #global_captures.remove(found)
      log.info("FOUND har response (remaining %s)", len(global_captures))
      response = found['response']
      return aiohttp.web.Response(body=response['data'], headers=response['headers'])
   else:
      log.info("ERROR: response for %s NOT FOUND", request.path)
      raise Exception("response for {request.path} NOT FOUND")
      
async def capture_request_response(request, response):
   body = await request.text()
   c = {
      'request':{
         'url':str(request.url),
         'path':str(request.path),
         'method':request.method,
         'headers':dict(request.headers),
         'data':body
      },
      'response':{
         'headers':dict(response.headers),
         'data':response.body},
   };
   global_captures.append(c)

def dict_rm(dct, key):
   if key in dct:
      dct.pop(key)
      
def mdict_to_dict(multi_dict):
   new_dict = {}
   for k in set(multi_dict.keys()):
      k = str(k)
      k_values = [str(v) for v in multi_dict.getall(k)]
      if len(k_values) > 1:
         new_dict[k] = [k for k in k_values]
      else:
         new_dict[k] = k_values[0]
   return new_dict


def get_capture():
   return global_capture
   
###################################################

import asyncio
import threading
from aiohttp import web

@pytest.fixture(scope='session')
def session_backend_emu(request):
   backend_emu = BackendEmu()
   loop = asyncio.new_event_loop() # loop created in main thread
   backend_emu.loop = loop
   def run_server(emu):
      asyncio.set_event_loop(loop)
      loop.run_until_complete(emu.run())
      emu.stop_event = asyncio.Event()
      loop.run_until_complete(emu.stop_event.wait())
      loop.run_until_complete(emu.close())
   t = threading.Thread(target=run_server, args=(backend_emu,))
   t.start()
   yield backend_emu
   backend_emu.write_coverage()
   backend_emu.stop_requested = True
   backend_emu.show_cpu()

   log.info('waiting for BackendEmu thread to exit...')
   t.join()
   log.info('BackendEmu thread done')
   

class BackendEmu:
   def __init__(self, port=None, use_https=False):
      self.mso = get_mso()
      self.use_https = use_https
      self.closed = False
      self.overrides = []
      self.runner = None
      self.web_app = None
      self.task = None
      self.rcvd_requests = []
      self.port = None
      self.stop_event = None
      self.stop_requested = None
      emu_responses.setup_graphql(self)
      emu_responses.setup_wexis(self)
      emu_responses.setup_srs(self)
      self.defaults = self.overrides
      self.started_utility_tasks = False
      self.ws_upd_task_infos = []
      self.psinfo = None
      
   def url_for(self, path="/"):
      return f"http://localhost:{self.port}{path}"
      
   async def run(self):
      log.info('BackendEmu.run on port %s', self.port)
      runner = None
      self.task = asyncio.create_task(self.start_site())
      self.periodic_task = asyncio.create_task(self.periodic(1, self.close_if_stop_requested))
      await asyncio.sleep(0.0)
      log.info('started BackendEmu on %s', self.port)

   async def close_if_stop_requested(self):
      if self.stop_requested:
         self.stop_event.set()
      return not self.stop_requested
      
   async def close(self):
      if not self.closed:
         log.info('BackendEmu.close')
         self.closed = True
         #await self.runner.cleanup() #takes 60 seconds
         self.task.cancel()
         await asyncio.wait([self.task])
         
   @staticmethod
   def utcnow():
      return pytz.utc.localize(datetime.utcnow())
   
   async def periodic(self, min_secs, func):
      """ run a function periodically at an exact time interval on the clock """
      continue_looping = True
      while continue_looping:
         secs = self.secs_into_day(self.utcnow())
         snooze = min_secs - secs % min_secs
         log.debug('snooze %.3f', snooze)
         await asyncio.sleep(snooze)
         try:
            continue_looping = await func()
         except:
            log.error('periodic func error: %s', self.get_stack())
         else:
            log.debug('periodic coro %s returned: %s', func.__name__, continue_looping)
      log.debug('periodic coro completed')
            
   @staticmethod
   def secs_into_day(dt_obj):
      return dt_obj.hour * 3600 + dt_obj.minute * 60 + dt_obj.second + dt_obj.microsecond / 1000000

   @staticmethod
   def get_stack():
      return ''.join(traceback.format_exception(*sys.exc_info()))
            
   def get_routes(self):
      mso = get_mso()
      # graphql_subscriptions_path = f'/{mso}/kairos/graphql/subscriptions'
      # wexis_path = 
      # wsupdates_path = f'/{mso}/kairos/wsupdates'
      # httpserver.expect_request(graphql_subscriptions_path).respond_with_handler(handle_request_graphql)
      # httpserver.expect_request(wsupdates_path).respond_with_handler(handle_request_wsupdates)
      # httpserver.expect_request("/").respond_with_handler(handle_request_srs)
      # httpserver.expect_request(srs_path).respond_with_handler(handle_request_srs)
      return [
         web.get(f"/ent/{mso}/fsm/assets/environment.json", self.serve_environment_json),
         web.get(f'/ent/{mso}/fsm/' + '{fpath:.*}', self.serve_static),
         web.get('/favicon.ico', self.serve_static),
         web.get(f'/{mso}/kairos/graphql/subscriptions', subscriptions),
         web.post(f'/{mso}/kairos/graphql', self.serve_graphql),
         web.post(f'/ent/{mso}/post.pl', self.serve_wexis),
         web.get(f'/{mso}/kairos/wsupdates', self.websocket_connect),
         web.get(f'/{mso}/kairos/' + '{anything:.*}', self.serve_srs),
         web.post(f'/{mso}/kairos/' + '{anything:.*}', self.serve_srs),
      ]

   async def serve_graphql(self, request):
      log.debug('graphql request url %s %s', request.path, request)
      return await self.get_response(request)

   async def serve_wexis(self, request):
      log.debug('wexis request url %s %s', request.path, request)
      return await self.get_response(request)

   async def serve_srs(self, request):
      log.info('srs request url %s %s', request.path, request)
      return await self.get_response(request)

   async def serve_environment_json(self, request):
      obj = {"serviceUri": self.url_for("").replace("http://", "")}
      #return aiohttp.web.Response(json=obj, headers={'Access-Control-Allow-Origin': '*'})
      return web.json_response(obj, headers={'Access-Control-Allow-Origin': '*'})
   
   ng_host_port = "localhost:4200"
   async def serve_static_ng(self, request):
      log.debug("static serve %s", request.url)
      forwarding_url = f'http://{self.ng_host_port}{request.path}'
      headers = mdict_to_dict(request.headers)
      requests_response = requests.request(
         request.method,
         url=forwarding_url,
         headers=headers,
         verify=False)
      log.info('rcvd response')
      rr = requests_response
      body = rr.content
      log.info('rcvd headers %s', headers)
      #log.info('rcvd content %s', body)
      headers = dict(rr.headers)
      log.info('body %s', body)
      #dict_rm(headers, 'Content-Encoding')
      #dict_rm(headers, 'Content-Length')
      #headers.update({'Access-Control-Allow-Origin': '*'})
      if re.match(r'\.js$', forwarding_url):
         headers['Content-Type'] = "application/javascript"
      log.info('rcvd headers %s', headers)
      return aiohttp.web.Response(body=body, headers=headers)

   def get_kk_dir(self):
      return os.environ.get('KK') or os.path.realpath(f"{os.getcwd()}/..")
   
   async def serve_static(self, request):
      # if not re.search(r'/login$', request.path):
      #    return await self.serve_static_ng(request)
      headers = {}
      mso = get_mso()
      fname = request.path.replace(f"/ent/{mso}/fsm", "")
      fname = re.sub(r'/login$', '/index.htm', fname)
      if re.match(r'.*\.js$', fname):
         content_type = "application/javascript"
      elif re.match('favicon.ico$', fname):
         content_type = "text/html; charset=iso-8859-1"
      elif re.match('.*svg$', fname):
         content_type = "image/svg+xml"
      elif re.match('.*css$', fname):
         content_type = "text/css"
      else:
         content_type = "text/html"
      serve_fname = None
      serve_dir = None
      kk_dir = self.get_kk_dir()
      if re.match(r'.*\.js$', fname) and os.environ.get('KK_COV'):
         serve_dir = kk_dir + "/coverage_dist"
      else:
         serve_dir = kk_dir + "/dist/kodename-kairos"
      serve_fname = f"{serve_dir}{fname}"
      log.info("static serve %s (%s)", serve_fname, content_type)
      assert os.path.exists(serve_fname)
      with open(serve_fname, mode="rb") as f:
         content = f.read()
      response = aiohttp.web.Response(
         body=content, content_type=content_type, headers={'Access-Control-Allow-Origin': '*'})
      return response

   async def websocket_connect(self, request):
      log.info('wsupdates request url %s %s', request.path, request)
      log.info('Websocket connection starting %s', request.query)
      mso = request.query.get('mso')
      ws_type = request.query.get('ws_type') or 'fm'
      if request.query.get('ent_prsn_id'):
         ent_prsn_id = int(request.query.get('ent_prsn_id'))
      else:
         ws_type = 'graphql/subscriptions'
         ent_prsn_id = 0
      wsi_kvs = dict(ent_prsn_id=ent_prsn_id)
      if ws_type == 'graphql/subscriptions':
         bu_id = 0
      elif ws_type == 'sqm':
         bu_id = 0
      else:
         bu_id = int(request.query.get('fc'))
         date_str = request.query.get('date')
         disp_filter = dict(TBD='tbd')
         wsi_kvs.update(dict(bu_id=bu_id, date_str=date_str, disp_filter=disp_filter))
      ws = web.WebSocketResponse(autoclose=False)
      ws_name = f'ws-{id(ws)}-{ws_type}'
      await ws.prepare(request)
      websocket_info = DotDict(
         ws=ws, name=ws_name, ws_type=ws_type, **wsi_kvs, channel='TBD')
      wsi = websocket_info
      container = self.get_container(ws_type)
      container[id(ws)] = wsi
      log.info('Websocket connection ready %s', wsi)
      if not self.started_utility_tasks:
         self.create_utility_tasks(wsi.ws_type)
      await self.rcv_client_msgs_loop(wsi)
      container.pop(id(ws))
      log.info('Websocket connection done %s', ws_name)

   def create_utility_tasks(self, ws_type):
      started_one = False
      log.info("GGG here")
      for info in self.ws_upd_task_infos:
         log.info("GGG here")
         if info.ws_type == ws_type:
            log.info("GGG here")
            started_one = True
            asyncio.create_task(self.send_ws_updates(info))
      if started_one:
         if self.psinfo:
            log.info("GGG here")
            asyncio.create_task(self.monitor_ps_info())
         self.started_utility_tasks = True
         
   async def monitor_ps_info(self):
      try:
         d = self.psinfo
         import psutil
         procs = list(psutil.process_iter())
         driver = next(filter(lambda p:re.match(".*chromedriver", str(p.name())), procs))
         ps = list(filter(lambda p:re.match(".*type=renderer", str(p.cmdline())), driver.children(recursive=True)))
         chrome = ps[-2]
         log.info("num chrome processes %s", len(ps))
         log.info("chrome process %s", chrome)
         await asyncio.sleep(d.start_after_seconds)
         log.info("begin cpu sampling")
         pct = chrome.cpu_percent()
         pcts = []
         for i in range(int(d.num_seconds * d.num_samples_per_second)):
            cpu_pct = chrome.cpu_percent()
            pcts.append(cpu_pct)
            await asyncio.sleep(1 / d.num_samples_per_second)
         d.pcts = pcts
         d.cpu_times = chrome.cpu_times()
         self.show_cpu()
      except Exception as e:
         log.info("EEEEEEEEEEEEEEEEEE %s", e)

   def show_cpu(self):
      if self.psinfo:
         d = self.psinfo
         pcts = d.pcts
         if pcts:
            log.info("cpu pct samples=%s, avg=%s, max=%s", len(pcts), round(sum(pcts) / len(pcts), 1), max(pcts))
            log.info("cpu pcts %s", pcts)
            log.info("cpu times %s", d.cpu_times)
      
   def setup_psinfo(self, num_seconds=10, num_samples_per_second=5, start_after_seconds=10):
      self.psinfo = DotDict(
         num_seconds=num_seconds,
         num_samples_per_second=num_samples_per_second,
         start_after_seconds=start_after_seconds,
         pcts = [])
      
   def setup_ws_updates(self, label='unk', num_seconds=120, num_updates_per_second=1, get_ws_upd_fn=None, start_after_seconds=10, ws_type='fm'):
      ws_upd_task_info = DotDict(
         label=label,
         num_seconds=num_seconds,
         num_updates_per_second=num_updates_per_second,
         start_after_seconds=start_after_seconds,
         get_ws_upd_fn=get_ws_upd_fn,
         ws_type=ws_type,
         completed=False)
      self.ws_upd_task_infos.append(ws_upd_task_info)

   def wait_for_ws_connect(self, ws_type='fm'):
      future = asyncio.run_coroutine_threadsafe(self.async_wait_for_ws_connect(ws_type), self.loop)
      result = future.result() # Wait for the result

   async def async_wait_for_ws_connect(self, ws_type):
      container = self.get_container(ws_type)
      log.info('waiting for %s ws connect...', ws_type)
      while not container:
         await asyncio.sleep(.1)
      
   async def send_ws_updates(self, info):
      log.info('%s ws upds started', info.label)
      await asyncio.sleep(info.start_after_seconds)
      for i in range(info.num_seconds * info.num_updates_per_second):
         ws_upd_msg = info.get_ws_upd_fn(i)
         for wsi in self.get_container(info.ws_type).values():
            json_msg = jsonify(ws_upd_msg)
            #log.info('sending %s %s', id(wsi.ws), json_msg) 
            await wsi.ws.send_str(json_msg)
            if i and not i % info.num_updates_per_second:
               log.info('sent %s %s %s ws upds %s', i, info.ws_type, info.label, ws_upd_msg)
            await asyncio.sleep(1/info.num_updates_per_second)
         if i % (5 * info.num_updates_per_second) == 0:
            await asyncio.sleep(1.2)
            
      info.completed = True
      log.info('%s ws upds completed', info.label)

   def send_ws_update(self, ws_upd_msg, ws_type='fm'):
      log.info("send %s ws_upd %s...", ws_type, ws_upd_msg[0])
      future = asyncio.run_coroutine_threadsafe(self.real_send_update(ws_upd_msg, ws_type), self.loop)
      result = future.result() # Wait for the result
      log.info("sent ws_upd %s", ws_upd_msg[0])
   
   def send_job_update(self, job):
      self.send_ws_update(['job', job.data])
   
   async def real_send_update(self, ws_upd_msg, ws_type):
      for wsi in self.get_container(ws_type).values():
         json_msg = jsonify(ws_upd_msg)
         log.info('sending %s %s %s', ws_type, id(wsi.ws), json_msg) 
         await wsi.ws.send_str(json_msg)

   def await_updates_completed(self):
      while not all([info.completed for info in self.ws_upd_task_infos]):
         log.debug('check for completed upds')
         time.sleep(1)

   def get_update_job(self, i):
      return fake.get_job(i % 501, t=2 if not i%2 else None)

   def get_container(self, ws_type):
      if not hasattr(self, 'websockets'):
         setattr(self, 'websockets', {})
      if not hasattr(self, 'sqm_websockets'):
         setattr(self, 'sqm_websockets', {})
      return self.sqm_websockets if ws_type == 'sqm' else self.websockets
      
   async def rcv_client_msgs_loop(self, wsi):
      async for msg in wsi.ws:
         log.debug("%s rcvd msg %s", wsi.name, msg)
         if msg.type != web.WSMsgType.TEXT:
            break

   async def start_site(self):
      log.info('BackendEmu.start_site')
      try:
         self.web_app = web.Application(middlewares=[self.middleware])
         self.web_app.add_routes(self.get_routes())
         log.info("use_https %s", self.use_https)
         ssl_kwargs = {}
         if self.use_https:
            ssl_kwargs = dict(ssl_context=get_ssl_context())
         self.runner = web.AppRunner(self.web_app)
         await self.runner.setup()
         port = 0
         site = web.TCPSite(self.runner, None, port, **ssl_kwargs)
         await site.start()
         host, port = site._server.sockets[0]._sock.getsockname()[0:2]
         log.info('BackendEmu serving on http://%s:%s', host, port)
         self.host = host
         self.port = port
         # import code
         # code.interact(local=locals())
      except Exception as e:
         log.error("EXCEPTION %s", str(e))

   from aiohttp.web import middleware
   @middleware
   async def middleware(self, request, handler):
      log.info('middleware %s', request.url)
      resp = await handler(request)
      return resp
   
   def set_response(self, matcher, value):
      if isinstance(matcher, str):
         matcher = {'rest_endpoint':matcher}
      self.overrides = [i for i in self.overrides if i.matcher != matcher]
      self.overrides.append(DotDict(matcher=matcher, value=value))

   async def forward_request(self, request):
      forwarding_host_port = "accdevxfsma0051.sp.csgidev.com:16758"
      forwarding_url = f'{request.scheme}s://{forwarding_host_port}{request.path}'
      log.info('FWD url %s', forwarding_url)
      headers = mdict_to_dict(request.headers)
      data = await request.read()
      requests_response = requests.request(
         request.method,
         url=forwarding_url,
         headers=headers,
         data=data,
         verify=False)
      log.info('rcvd response')
      rr = requests_response
      body = rr.content
      #log.info('rcvd content %s', body)
      headers = dict(rr.headers)
      log.info('FWD payload %s', data)
      dict_rm(headers, 'Transfer-Encoding')
      dict_rm(headers, 'Content-Encoding')
      dict_rm(headers, 'Content-Length')
      dict_rm(headers, 'Content-Type')
      log.info('FWD rcvd headers %s', headers)
      log.info('FWD rcvd body %s', body)
      response = aiohttp.web.Response(body=body, content_type='application/json', headers=headers)
      return response

   async def get_response(self, request):
      self.rcvd_requests.append(request)
      response = None
      log.info('decoded request %s', decode_body(await request.text()) or request.url)
      if not global_twebui_forward: # and not await self.request_is_wexis_login(request):
         response = await self.get_emu_data_response(request)
      if global_twebui_forward and response is None:
         response = await self.forward_request(request)
         await capture_request_response(request, response)
      return response

   async def request_is_wexis_login(self, request):
      text = await request.text()
      body = decode_body(text)
      return body and funcy.project(body, ["rp", "path"]) == {"rp":"EntWexis::wp_wexis_login", "path":"1.0/login"}
   
   async def get_emu_data_response(self, request):
      r = None
      text = await request.text()
      body = decode_body(text)
      for emu_data in self.overrides + self.defaults:
         #print("try", request.url, emu_data)
         match_endpoint = emu_data.matcher.get('rest_endpoint')
         if match_endpoint:
            if str(request.url).endswith(match_endpoint) or f"{match_endpoint}?" in str(request.url):
               r = await self.response_data_from_value(emu_data.value)
               break
         elif body:
            subset = funcy.project(body, list(emu_data.matcher.keys()))
            if subset == emu_data.matcher:
               r = await self.response_data_from_value(emu_data.value)
               break
      response = None
      if isinstance(r, aiohttp.web.HTTPException):
         response = aiohttp.web.Response(status=r.status, text=jsonify(r.reason),
                                         content_type="application/json")
      elif r is not None:
         #response = web.json_response(r)
         response = aiohttp.web.Response(text=jsonify(r), content_type="application/json")
      log.info('emu response %s', response)
      return response

   async def response_data_from_value(self, value):
      rdata = None
      if inspect.iscoroutinefunction(value):
         rdata = await value()
      elif callable(value):
         rdata = value()
      else:
         rdata = value
      return rdata
   
   def write_coverage(self):
      """
      https://medium.com/@oresoftware/front-end-javascript-test-coverage-with-istanbul-selenium-372b3292733
      """
      if os.environ.get('KK_COV'):
         driver = sb_config.shared_driver
         log.info("sb_config %s", driver) # only here with --rs option
         log.info("sb_config %s", sb_config) # only here with --rs option
         log.info("sb_config %s", vars(sb_config)) # only here with --rs option
         log.info("downloading coverage data...")
         driver.set_script_timeout(90)
         coverage = driver.execute_script("return window.__coverage__;")
         kk_dir = self.get_kk_dir()
         nyc_dir = f"{kk_dir}/.nyc_output"
         if not os.path.exists(nyc_dir):
            os.makedirs(nyc_dir)
         fname = f"{nyc_dir}/coverage.json"
         import json
         with open(fname, 'w') as f:
            json.dump(coverage, f)
         log.info("wrote %s", fname)

@pytest.fixture()
def backend_emu(session_backend_emu):
   emu = session_backend_emu
   emu.overrides = []
   emu.rcvd_requests = []
   yield emu


