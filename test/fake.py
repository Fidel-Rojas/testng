import logging;
import functools
from datetime import datetime, time
import pytz
from faker import Faker
from fsm_gui_data_packets \
   import Job, Tech, Tsh, TechGps, JobFilterData, TechStatus, FilterSession, JobResched, \
   SkillGroup, KeyValueMapping, CapacityCategory, Timeslot, TechFilteredOut, SupportTask
import define_packets
import fsm_tdm


log = logging.getLogger(__name__)
idg = fsm_tdm.IdGen(39)
Faker.seed(217)
faker = Faker()
define_packets.setup_fields()

util_utcnow = datetime.utcnow()
def utcnow():
    d = util_utcnow
    return d
       
@functools.lru_cache(None)
def last_name(t):
    return faker.last_name()

@functools.lru_cache(None)
def first_name(t):
    return faker.first_name()

def get_job(w, **kwargs):
    job = job_defaults(w)
    job.update(kwargs)
    return Job.from_dict(job)
    
def get_tech(t, **kwargs):
    tech = tech_defaults(t)
    tech['tech_shifts'][0].update(kwargs.get('tsh', {}))
    tech.update(kwargs)
    return Tech.from_dict(tech)
   
def jobs(num, get_job_fn=get_job):
    # return jobs_result(get_job_fn(i) for in in range) This?
    rows = []
    for i in range(num):
        rows.append(get_job_fn(i).data)
    return {"rows":rows, "columns":Job.fields}

def jobs_result(jobs):
    rows = [job.data for job in jobs]
    return {"rows":rows, "columns":Job.fields}

def techs(num):
    rows = []
    for i in range(num):
        rows.append(get_tech(i).data)
    return {"rows":rows, "columns":Tech.fields, "shiftColumns":Tsh.fields}
   
def techs_result(techs):
    rows = [tech.data for tech in techs]
    return {"rows":rows, "columns":Tech.fields, "shiftColumns":Tsh.fields}
   
def get_support_task(st, **kwargs):
    support_task = support_task_defaults(st)
    t = kwargs.get('tech')
    if t is not None:
        tech = get_tech(t)
        support_task.update(dict(
            tech_id=tech.tech_id,
            tech_first_name=tech.tech_first_name,
            tech_last_name=tech.tech_last_name,
            tech_num=tech.tech_num))
    w = kwargs.get('w')
    if w is not None:
        support_task.update(dict(
            job_number=idg.job_number(w),
            work_order_number=idg.wonum(w),))        
    support_task.update(kwargs)
    return SupportTask.from_dict(support_task)

def support_tasks_result(support_tasks):
    rows = [st.data for st in support_tasks]
    return {"rows":rows, "columns":SupportTask.fields}
   
def job_defaults(w):
   d = {
       'apartment': None,
       'bu_id': idg.bu_id(),
       'building': None,
       'city': 'DENVER',
       'company_id': None,
       'cust_name': faker.name(),
       'customer_type': 'RESIDENTIAL',
       'dispatcher_status': 'O',
       'duration_mins': None,
       'end_time': None,
       'entity_timestamp': utcnow(),
       'est': None,
       'estimated_points': 12,
       'eta': None,
       'event_types': [],
       'job_number': idg.job_number(w),
       'job_state': 'UA',
       'latitude': 39.666667,
       'longitude': -104.872903,
       'ordered': None,
       'project_job_code': None,
       'service_area': 'GRP-RT',
       'working_area': 'GRP-RT',
       'skill_codes': ['SK'],
       'specialty_skill_count': 0,
       'start_time': None,
       'state': 'CO',
       'street_name': faker.street_address(),
       'street_name_2': None,
       'tech_first_name': None,
       'tech_id': None,
       'tech_last_name': None,
       'tech_num': None,
       'timeslot_code': idg.get_ts_info(818).abbr,
       'timeslot_id': idg.timeslot_id(818),
       'vip_cd': 'Y',
       'work_order_class': 'T',
       'order_num': None,
       'work_order_number': idg.wonum(w),
       'work_order_type': 'COMPONENT-SK',
       'zip_code_plus': faker.postcode(),
       'oose_ids': [],
       'oose_errors': [],
       'tags': [],
       'orig_estimated_points': 4,
       'routing_criteria': 'RT'}
   return d

def tech_defaults(t=0):
    d = {
        'tech_id': idg.tech_id(t),
        'tech_num': idg.tech_num(t),
        'has_association_conflict': None,
        'company_id': None,
        'employee_flag': 'EMP',
        'entity_timestamp': utcnow(),
        'event_types': [],
        'expiration_datetime': None,
        'fsup_tech_id': None,
        'fsup_tech_first_name': None,
        'fsup_tech_last_name': None,
        'isa_associated_tech': None,
        'isa_fsup': None,
        'isa_primary_tech': None,
        'latitude': None,
        'longitude': None,
        'num_tschev_unordered': None,
        'office_name': None,
        'pool_type': None,
        'tech_status_datetime': utcnow(),
        'last_chg_datetime': utcnow(),
        'tech_status_cd': 'AV',
        'tech_first_name': first_name(t),
        'tech_last_name': last_name(t),
        'time_offset_from_utc': get_time_offset_from_utc(utcnow()),
        'title': None,
        'tech_shifts': [tsh_defaults(t)],
    }
    return d

def support_task_defaults(st):
    return dict(
        st_id=idg.id(st),
        flctr_bu_id=idg.bu_id(0),
        std_id=idg.id(0),
        ent_prsn_id=None,
        category='Needs Backup',
        subject='Please Help',
        body='support task message here',
        status='INQUEUE',
        create_dt=datetime(2023,1,1,12),
        accept_dt=None,
        complete_dt=None,
        new_message_cnt=None,
        notes='support task notes here',
        st_actn_type_cd=None,
        tech_id=None,
        tech_first_name=None,
        tech_last_name=None,
        tech_num=None,
        job_number=None,
        work_order_number=None,)

default_tzname = 'US/Mountain'
def get_time_offset_from_utc(date):
    date_noon = datetime.combine(date, time(12, 0, 0))
    tz_name = default_tzname
    offset = pytz.timezone(tz_name).localize(date_noon).strftime('%z')
    return offset[:3] + ':' + offset[3:]

def tsh_defaults(t):
    d = {
        'break_begin_time': None,
        'break_end_time': None,
        'current_fulfill_center_bu_id': idg.bu_id(),
        'daily_override_ind': 'N',
        'latitude': None,
        'longitude': None,
        'routing_criterias': ['GRP-RT'],
        'shift_begin_time': time(8).isoformat('minutes'),
        'shift_end_time': time(17).isoformat('minutes'),
        'shift_type_cd': 'WORK',
        'skill_codes': ['SK'],
        'wcam_id': None,
        'work_day_ind': 'Y',
        'wsrm_id': None,
        'tech_shift_id': idg.id(t)
    }
    return d

def get_timeslots():
    return [
        {"timeslot_id":39000810,"timeslot_abbr":"IK","timeslot_desc":"08-10","timeslot_begin_time":"2023-02-23T13:00:00","timeslot_end_time":"2023-02-23T15:00:00"},
        {"timeslot_id":39001012,"timeslot_abbr":"KM","timeslot_desc":"10-12","timeslot_begin_time":"2023-02-23T15:00:00","timeslot_end_time":"2023-02-23T17:00:00"},
        {"timeslot_id":39001214,"timeslot_abbr":"MO","timeslot_desc":"12-14","timeslot_begin_time":"2023-02-23T17:00:00","timeslot_end_time":"2023-02-23T19:00:00"},
        {"timeslot_id":39001416,"timeslot_abbr":"OQ","timeslot_desc":"14-16","timeslot_begin_time":"2023-02-23T19:00:00","timeslot_end_time":"2023-02-23T21:00:00"},
        {"timeslot_id":39001618,"timeslot_abbr":"QS","timeslot_desc":"16-18","timeslot_begin_time":"2023-02-23T21:00:00","timeslot_end_time":"2023-02-23T23:00:00"},
        {"timeslot_id":39000812,"timeslot_abbr":"IM","timeslot_desc":"08-12","timeslot_begin_time":"2023-02-23T13:00:00","timeslot_end_time":"2023-02-23T17:00:00"},
        {"timeslot_id":39000818,"timeslot_abbr":"IS","timeslot_desc":"08-18","timeslot_begin_time":"2023-02-23T13:00:00","timeslot_end_time":"2023-02-23T23:00:00"},
        {"timeslot_id":39001218,"timeslot_abbr":"MS","timeslot_desc":"12-18","timeslot_begin_time":"2023-02-23T17:00:00","timeslot_end_time":"2023-02-23T23:00:00"},
        {"timeslot_id":0,"timeslot_abbr":"N","timeslot_desc":"NSCHD","timeslot_begin_time":"1995-08-24T00:00:00","timeslot_end_time":"1995-08-25T05:00:00"},
        {"timeslot_id":-3,"timeslot_abbr":"-3","timeslot_desc":"Self Provisioned","timeslot_begin_time":"1995-08-24T00:00:00","timeslot_end_time":"1995-08-25T06:00:00"},
        {"timeslot_id":-1,"timeslot_abbr":"U","timeslot_desc":"UNKNWN","timeslot_begin_time":"1995-08-24T00:00:00","timeslot_end_time":"1995-08-25T05:00:00"}
    ]

def get_timeslot(timeslot_id):
    return next(filter(lambda ts: ts['timeslot_id'] == timeslot_id, get_timeslots()))

def dspch_msg(m, **kwargs):
    dspch_msg = dspch_msg_defaults(m)
    w = kwargs.get('w')
    if w is not None:
        job = get_job(w)
        dspch_msg.update(dict(
            routing_criteria=job.routing_criteria,
            work_order_number=job.work_order_number,))        
    dspch_msg.update(kwargs)
    return DspchMsg.from_dict(dspch_msg)

def dspch_msg_defaults(m):
    return dict(
        message_id=idg.id(m),
        bu_id=idg.bu_id(0),
        sender_prsn_id=-1,
        subject=f'Dspch Msg {m} Subject',
        type_code='AA',
        body='This is a message body',
        create_datetime=Util.utcnow(),
        last_chg_oper='JJ',
        last_chg_datetime=Util.utcnow(),
        accept_datetime=None,
        accept_prsn_id=None,
        complete_datetime=None,
        site_level_ind="N",
        parent_message_id=None,
        cancel_dt=None,)
