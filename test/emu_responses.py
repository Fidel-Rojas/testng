import fake
null = None
false = False
true = True

####################################
def setup_srs(emu):
    emu.set_response("/techs", lambda:fake.techs_result([]))
    emu.set_response("/jobs", lambda:fake.jobs_result([]))
    emu.set_response('/headers', get_headers)
    emu.set_response('/available_bus', get_available_bus)
    emu.set_response('/mapping/skill-groups', get_skill_groups)
    emu.set_response('/mapping/timeslots', get_timeslots)
    emu.set_response('/mapping/WORK_ORDER_TYPES_CD', get_work_order_type_cds)
    emu.set_response('/mapping/TECH_ALERT_CD', get_tech_alert_cds)
    emu.set_response('/mapping/JOB_STATE_CD', get_job_state_cds)
    emu.set_response('/mapping/WO_ALERT_CD', get_wo_alert_cds)
    emu.set_response('/mapping/DISP_TO_BSS_STATUS_MAPPING', get_disp_to_bss_status_mapping)
    emu.set_response('/mapping/ASSIGNMENT_TYPE_CD', get_assignment_type_cd)
    emu.set_response('/support-tasks', lambda:fake.support_tasks_result([]))
    emu.set_response('/web-layout-properties', get_web_layout_properties)

def get_available_bus():
    return {
        "dvsns":[
            {"id":3920,"desc":"dvsn3920",
             "sites":[
                 {"id":3910,"desc":"site3910","qualifier":"Q",
                  "fcs":[
                      {"id":3900,"desc":"flctr3900","hasAccess":true},
                      {"id":3901,"desc":"flctr3901","hasAccess":true},
                      {"id":3902,"desc":"flctr3902","hasAccess":true},
                      {"id":3903,"desc":"flctr3903","hasAccess":true}],
                  "hasAccess":true}],
             "hasAccess":true}]}

def get_timeslots():
    return fake.get_timeslots()
    
def get_skill_groups():
    return [
        {"skill_code":"AUTOSCHED-WOT-1","skill_desc":"AUTOSCHED-WOT-1-desc","skill_priority":99},
        {"skill_code":"SK","skill_desc":"SK-desc","skill_priority":99},
        {"skill_code":"SL","skill_desc":"SL-desc","skill_priority":99},
        {"skill_code":"SM","skill_desc":"SM-desc","skill_priority":99},
        {"skill_code":"SN","skill_desc":"SN-desc","skill_priority":99},
        {"skill_code":"SQ","skill_desc":"SQ-desc","skill_priority":99},
        {"skill_code":"SR","skill_desc":"SR-desc","skill_priority":99},
    ]

def get_disp_to_bss_status_mapping():
    return [
        {"code":"O","description":"AS"},
        {"code":"C","description":"CP"},
    ]

def get_assignment_type_cd():
    return [
        {"code":"M","description":"Manual"},
        {"code":"A","description":"Autoroute"},
        {"code":"D","description":"Autodispatch"},
    ]

def get_work_order_type_cds():
    return [
        {"code":"AUTOSCHED-WOT-1","description":"skill AUTOSCHED-WOT-1"},
        {"code":"DC","description":"skill DC"},
        {"code":"NC","description":"skill NC"},
        {"code":"SK","description":"skill SK"},
        {"code":"SL","description":"skill SL"},
        {"code":"SM","description":"skill SM"},
        {"code":"SN","description":"skill SN"},
        {"code":"SQ","description":"skill SQ"},
        {"code":"SR","description":"skill SR"},
        {"code":"ST","description":"skill ST"},
    ]

def get_tech_alert_cds():
    return [
        {"code":"ADGAP1","description":"AD can not locate a job for the Tech"},
        {"code":"ADGAP2","description":"Tech needs next AD job"},
        {"code":"ADGAP3","description":"AD can not fill gap for Tech"},
        {"code":"ADGAP4","description":"Gap shift threshold"},
        {"code":"DEVICECOMM-N","description":"Communication with TechNet"},
        {"code":"EQ-S","description":"Tech with Insufficient equipment"},
        {"code":"JOBINFNOTONSHIFT","description":"Job Information Not On Shift"},
        {"code":"TS-AVAIL","description":"Tech Status Running Long"},
        {"code":"TS-BREAK","description":"Tech Status Running Long"},
        {"code":"TS-ENROUTE","description":"Tech Status Running Long"},
        {"code":"TS-LUNCH","description":"Tech Status Running Long"},
        {"code":"TS-WRAPUP","description":"Tech Status Running Long"},
    ]

def get_job_state_cds():
    return [
        {"code":"AS","description":"Assigned"},
        {"code":"CN","description":"Cancelled"},
        {"code":"CP","description":"Complete"},
        {"code":"IS","description":"In Service"},
        {"code":"ND","description":"Held"},
        {"code":"UA","description":"Unassigned"},
    ]

def get_wo_alert_cds():
    return [
        {"code":"ADJOBLOCK-EOS","description":"EOS - AD Lock Job"},
        {"code":"ADJOBLOCK-STC-LUNCH","description":"STC - AD Lock Job"},
        {"code":"DELJOBSPENDPAY","description":"Delinquent Jobs with Pending Payments"},
        {"code":"ESTTS","description":"EST Timeslot Jeopardy"},
        {"code":"FAILJOBS","description":"Failed Jobs"},
        {"code":"JCL","description":"Job Completing Late"},
        {"code":"JOWNONWORKSHIFT","description":"Jobs with non working techs"},
        {"code":"JSL","description":"Job Start Late"},
        {"code":"META","description":"Missed ETA"},
        {"code":"NM","description":"Near Miss"},
        {"code":"NOLATLONG","description":"No Lat Longs"},
        {"code":"OV","description":"Overrun"},
        {"code":"SPHANDLJOB","description":"Special Handling Job"},
        {"code":"UNASS","description":"Unassigned"},
        {"code":"VIP-UNASS","description":"VIP Unassigned"},
    ]

def get_headers():
    return {
        "job":["work_order_number","order_num","job_number","bu_id","job_state","work_order_type","est","estimated_points","eta","work_order_class","working_area","service_area","dispatcher_status","timeslot_code","timeslot_id","duration_mins","start_time","end_time","company_id","ordered","tech_id","tech_num","tech_first_name","tech_last_name","project_job_code","cust_name","customer_type","vip_cd","street_name","street_name_2","building","apartment","city","state","zip_code_plus","latitude","longitude","specialty_skill_count","skill_codes","event_types","oose_ids","oose_errors","tags","orig_estimated_points","routing_criteria","entity_timestamp"],
        "tech":["tech_id","tech_num","company_id","employee_flag","latitude","longitude","pool_type","tech_status_cd","tech_first_name","tech_last_name","fsup_tech_id","fsup_tech_first_name","fsup_tech_last_name","isa_fsup","expiration_datetime","title","office_name","isa_primary_tech","isa_associated_tech","has_association_conflict","num_tschev_unordered","time_offset_from_utc","event_types","tech_shifts","entity_timestamp","tech_status_datetime","last_chg_datetime"],
        "tsh":["break_begin_time","break_end_time","current_fulfill_center_bu_id","daily_override_ind","latitude","longitude","shift_begin_time","shift_end_time","shift_type_cd","wcam_id","work_day_ind","wsrm_id","routing_criterias","skill_codes","tech_shift_id"],
        "tech_filtered_out":["tech_id"],
        "tech_gps":["tech_id","latitude","longitude","motion_status","entity_timestamp"],
        "tech_status":["tech_id","tech_status_cd","tech_status_datetime"],
        "job_filter_data":["bu_id","tech_id","skill_codes","working_area","timeslot_id","company_id"],
        "job_resched":["work_order_number","entity_timestamp"],
        "skill_group":["skill_code","skill_desc","skill_priority"],
        "key_value_mapping":["key","value"],
        "capacity_category":["wsc_id","sched_category","wcam_id"],
        "timeslot":["timeslot_id","timeslot_abbr","timeslot_desc","timeslot_begin_time","timeslot_end_time"],
        "filter_session":["ent_filter_session_id"],
        "support_task":["st_id","job_number","work_order_number","std_id","flctr_bu_id","ent_prsn_id","category","subject","body","tech_id","tech_first_name","tech_last_name","tech_num","status","create_dt","accept_dt","complete_dt","new_message_cnt","notes","st_actn_type_cd"]
    }

def get_web_layout_properties():
    return [
      {
         'suite': 'FSM', 
         'main_frame': 'fulfillmentManagement', 
         'component': 'jobList', 
         'name': 'jobPriority', 
         'meta_data': None, 
         'should_display_in_ui': False
      }, {
         'suite': 'FSM', 
         'main_frame': 'fulfillmentManagement', 
         'component': 'jobList', 
         'name': 'skillPriority', 
         'meta_data': None, 
         'should_display_in_ui': False
      }, {
         'suite': 'FSM', 
         'main_frame': 'fulfillmentManagement', 
         'component': 'jobList.address', 
         'name': 'node', 
         'meta_data': None, 
         'should_display_in_ui': False
      }
   ]

####################################
def setup_graphql(emu):
    emu.set_response({"operationName":"getTechEvents"}, {"data":{"getTechEvents":[]}})
    emu.set_response({"operationName":"fillDispatchMessagesTechV2"}, {"data":{"result":"Success"}})
    emu.set_response({"operationName":"getSSOConfig"}, {"data":{"getSSOConfig":{"ssoAuth":"OFF","authServiceUrl":null,"__typename":"SSOConfig"}}})
    emu.set_response({"operationName":"appFunction"}, get_app_function_response)
    emu.set_response({"operationName":"getJobAssigmentPlans"}, {"data":{"getJobAssigmentPlans":[]}})
    emu.set_response({"operationName":"getJobsByFilters"}, {"data":{"getJobsByFilters":[]}})
    emu.set_response({"operationName":"supportTaskCounts"}, {"data":{"supportTaskCounts":{"accepted":0,"queued":0,"__typename":"SupportTaskCounts"}}})
    emu.set_response({"operationName":"getFulfillmentCenterData"}, get_fulfillment_center_data_response)
    emu.set_response({"operationName":"getDispatchMessages"}, {"data":{"getDispatchMessages":[]}})
    emu.set_response({"operationName":"removeDispatchMessagesTechV2"}, {"data":{"result":"Success","removeDispatchMessagesTechV2":[]}})
    emu.set_response({"operationName":"getTechList"}, {"data":{"getFiltersTechsMegaList":[]}})

def get_fulfillment_center_data_response():
   return {"data":{"getMsoFulFillmentCenters":[
      {"id":3900,"timezoneName":"America/New_York","defaultGps":{"latitude":39.741209,"longitude":-104.989427,"__typename":"GpsData"},"customIniSettings":{"requireManualAssignmentReason":false,"usesPreferredTechFunctionality":false,"__typename":"FulFillmentCenterCustomIniSettings"},"__typename":"FulFillmentCenter"},
      {"id":3901,"timezoneName":"America/New_York","defaultGps":{"latitude":39.741209,"longitude":-104.989427,"__typename":"GpsData"},"customIniSettings":{"requireManualAssignmentReason":false,"usesPreferredTechFunctionality":false,"__typename":"FulFillmentCenterCustomIniSettings"},"__typename":"FulFillmentCenter"},
      {"id":3902,"timezoneName":"America/New_York","defaultGps":{"latitude":39.741209,"longitude":-104.989427,"__typename":"GpsData"},"customIniSettings":{"requireManualAssignmentReason":false,"usesPreferredTechFunctionality":false,"__typename":"FulFillmentCenterCustomIniSettings"},"__typename":"FulFillmentCenter"},
      {"id":3903,"timezoneName":"America/New_York","defaultGps":{"latitude":39.741209,"longitude":-104.989427,"__typename":"GpsData"},"customIniSettings":{"requireManualAssignmentReason":false,"usesPreferredTechFunctionality":false,"__typename":"FulFillmentCenterCustomIniSettings"},"__typename":"FulFillmentCenter"}
   ]}}
      
def get_app_function_response():
   return {"data":{"modulesByPersonId":[
      {"afId":100,"parentAfId":null,"name":"Enterprise Management","description":"Workforce Enterprise Management","displayOrder":1,"__typename":"AppFunction"},
      {"afId":101,"parentAfId":100,"name":"Administration","description":"WEM Customer Administration wrapper","displayOrder":1,"__typename":"AppFunction"},
      {"afId":102,"parentAfId":104,"name":"Contractor Companies","description":"Creation/management of contractor companies across the entire MSO","displayOrder":3,"__typename":"AppFunction"},
      {"afId":103,"parentAfId":101,"name":"Personnel","description":"Personnel window for Enterprise","displayOrder":1,"__typename":"AppFunction"},
      {"afId":104,"parentAfId":101,"name":"MSO Configuration","description":"Configuration pages for Enterprise level settings","displayOrder":3,"__typename":"AppFunction"},
      {"afId":105,"parentAfId":104,"name":"Amdocs Configuration","description":"Pages allowing the market to define codes needed in WFX that are not sent to WFX from DDP.  Users enter them in at the ENT level and are pushed down to WFX.","displayOrder":1,"__typename":"AppFunction"},
      {"afId":106,"parentAfId":104,"name":"Events for Notification","description":"Page allowing the administrator to define which consumers and events for which they will receive WFX notifications","displayOrder":5,"__typename":"AppFunction"},
      {"afId":107,"parentAfId":104,"name":"Password Configuration","description":"Configuration of how passwords are used across the entire MSO","displayOrder":8,"__typename":"AppFunction"},
      {"afId":108,"parentAfId":101,"name":"Scorecard Configuration","description":"Functionality that allows the market to configure which scorecards to display and how the metrics are measured","displayOrder":4,"__typename":"AppFunction"},
      {"afId":109,"parentAfId":101,"name":"Custom Solutions","description":"Page only displayed for specific MSO and holds their configuration for custom solutions","displayOrder":5,"__typename":"AppFunction"},
      {"afId":110,"parentAfId":100,"name":"Scorecard","description":"Pulls data from core WFX to display in different \"chips\" with drill down logic","displayOrder":3,"__typename":"AppFunction"},
      {"afId":111,"parentAfId":100,"name":"Reports","description":"Executive reports","displayOrder":2,"__typename":"AppFunction"},
      {"afId":112,"parentAfId":100,"name":"Support Task Management","description":"Page allowing user to accept, reply to and complete tasks","displayOrder":4,"__typename":"AppFunction"},
      {"afId":113,"parentAfId":104,"name":"Support Task Configuration","description":"Configuration pages related to defining how support tasks will work in ENT and core WFX","displayOrder":10,"__typename":"AppFunction"},
      {"afId":114,"parentAfId":104,"name":"Scorecard Filter Configuration","description":"Defines filters the MSO wants used by their team for Scorecard","displayOrder":9,"__typename":"AppFunction"},
      {"afId":115,"parentAfId":101,"name":"Site Configuration","description":"Configuration pages defining settngs for an entire site","displayOrder":5,"__typename":"AppFunction"},
      {"afId":116,"parentAfId":104,"name":"General Configuration","description":"Configuration page holds general configuration used throughout the MSO, items that cannot be categorized into another page.","displayOrder":6,"__typename":"AppFunction"},
      {"afId":117,"parentAfId":104,"name":"Dispatch Operations Filter","description":"Page allows user to create filters the R and D users will use, replacing the R and Ds saved filters","displayOrder":4,"__typename":"AppFunction"},
      {"afId":118,"parentAfId":104,"name":"Office Location","description":"Page allows the administrator to define offices and determine which fulfillment centers in which the offices are being used.","displayOrder":7,"__typename":"AppFunction"},
      {"afId":119,"parentAfId":104,"name":"Where's My Tech Configuration","description":"Page allowing user to configure fields for display in WMT pages","displayOrder":11,"__typename":"AppFunction"},
      {"afId":123,"parentAfId":100,"name":"Dashboard/Control Center","description":"Dashboards to monitor Tasks, WFX Alerts and WFX Capacity.","displayOrder":2,"__typename":"AppFunction"},
      {"afId":124,"parentAfId":100,"name":"Data Importer","description":"Page allowing user to import data into WFX databases","displayOrder":5,"__typename":"AppFunction"},
      {"afId":125,"parentAfId":100,"name":"Kairos","description":"Kairos, the next generation of Workforce Management","displayOrder":6,"__typename":"AppFunction"},
      {"afId":126,"parentAfId":125,"name":"Fulfillment","description":"Kairos fulfillment page. Used by dispatchers to help technicians during the work day","displayOrder":1,"__typename":"AppFunction"},
      {"afId":127,"parentAfId":125,"name":"Support Tasks","description":"Kairos Support Tasks. To be used by Support Tasks Agents","displayOrder":6,"__typename":"AppFunction"},
      {"afId":128,"parentAfId":125,"name":"Resource Allocation","description":"Kairos Resource Allocation.  To be used by Resource Allocation Managers","displayOrder":3,"__typename":"AppFunction"},
      {"afId":129,"parentAfId":104,"name":"Activity Definition","description":"Page allows the MSO to create activity pick lists that can be used in TechNet and WFX GUI for adding activites to jobs","displayOrder":2,"__typename":"AppFunction"},
      {"afId":130,"parentAfId":125,"name":"Settings & Configuration","description":"FSM Settings and Configuration to be used by and Admin person","displayOrder":5,"__typename":"AppFunction"},
      {"afId":133,"parentAfId":130,"name":"Normalization","description":"Give Access to BRE-Normalization tab","displayOrder":1,"__typename":"AppFunction"},
      {"afId":134,"parentAfId":130,"name":"Business Rule","description":"Give Access to BRE-Busines Rule tab","displayOrder":2,"__typename":"AppFunction"},
      {"afId":135,"parentAfId":130,"name":"Workflow","description":"Give access to BRE-Workflow tab","displayOrder":3,"__typename":"AppFunction"},
      {"afId":136,"parentAfId":130,"name":"Event Management","description":"Give access to BRE Event Management","displayOrder":4,"__typename":"AppFunction"},
      {"afId":137,"parentAfId":130,"name":"Service Api","description":"Give access to BRE Service API","displayOrder":4,"__typename":"AppFunction"},
      {"afId":138,"parentAfId":133,"name":"[ Action: Create ]","description":"Admin person can create Normalization","displayOrder":1,"__typename":"AppFunction"},
      {"afId":139,"parentAfId":133,"name":"[ Action: Enable ]","description":"Admin person can enable Normalization","displayOrder":2,"__typename":"AppFunction"},
      {"afId":140,"parentAfId":133,"name":"[ Action: Disable ]","description":"Admin person can disable Normalization","displayOrder":3,"__typename":"AppFunction"},
      {"afId":141,"parentAfId":133,"name":"Normalization Details","description":"Admin person has access to Normalization Detail","displayOrder":4,"__typename":"AppFunction"},
      {"afId":142,"parentAfId":141,"name":"Normalization Summary","description":"Admin person has access to Summary Section on Detail Normalization","displayOrder":1,"__typename":"AppFunction"},
      {"afId":143,"parentAfId":141,"name":"Normalization Management","description":"Admin person has access to Management Section on Detail Normalization","displayOrder":2,"__typename":"AppFunction"},
      {"afId":144,"parentAfId":142,"name":"[ Action: Edit ]","description":"Admin person can edit on Normalization Summary Section","displayOrder":1,"__typename":"AppFunction"},
      {"afId":145,"parentAfId":142,"name":"[ Action: Status Update ]","description":"Admin person can change status on Normalization Summary Section","displayOrder":2,"__typename":"AppFunction"},
      {"afId":146,"parentAfId":143,"name":"[ Action: Create ]","description":"Admin person can create Normalization on Management Section","displayOrder":1,"__typename":"AppFunction"},
      {"afId":147,"parentAfId":143,"name":"[ Action: Edit ]","description":"Admin person can edit Normalization on Management Section","displayOrder":2,"__typename":"AppFunction"},
      {"afId":148,"parentAfId":143,"name":"[ Action: Remove ]","description":"Admin person can remove Normalization on Management Section","displayOrder":3,"__typename":"AppFunction"},
      {"afId":149,"parentAfId":143,"name":"[ Action: Save ]","description":"Admin person can save Normalization on Management Section","displayOrder":4,"__typename":"AppFunction"},
      {"afId":150,"parentAfId":134,"name":"[ Action: Create ]","description":"Admin person can create Business Rule","displayOrder":1,"__typename":"AppFunction"},
      {"afId":151,"parentAfId":134,"name":"[ Action: Enable ]","description":"Admin person can enable Business Rule","displayOrder":2,"__typename":"AppFunction"},
      {"afId":152,"parentAfId":134,"name":"[ Action: Disable ]","description":"Admin person can disable Business Rule","displayOrder":3,"__typename":"AppFunction"},
      {"afId":153,"parentAfId":134,"name":"Business Rule Details","description":"Admin person has access to Business Rule Detail","displayOrder":4,"__typename":"AppFunction"},
      {"afId":154,"parentAfId":153,"name":"Business Rule Summary","description":"Admin person has access to Summary Section on Detail Business Rule","displayOrder":1,"__typename":"AppFunction"},
      {"afId":155,"parentAfId":153,"name":"Business Rule Management","description":"Admin person has access to Management Section on Detail Business Rule","displayOrder":2,"__typename":"AppFunction"},
      {"afId":156,"parentAfId":154,"name":"[ Action: Edit ]","description":"Admin person can edit on Business Rule Summary Section","displayOrder":1,"__typename":"AppFunction"},
      {"afId":157,"parentAfId":154,"name":"[ Action: Status Update ]","description":"Admin person can change status on Business Rule Summary Section","displayOrder":2,"__typename":"AppFunction"},
      {"afId":158,"parentAfId":155,"name":"[ Action: Create ]","description":"Admin person can create Business Rule on Management Section","displayOrder":1,"__typename":"AppFunction"},
      {"afId":159,"parentAfId":155,"name":"[ Action: Edit ]","description":"Admin person can edit Business Rule on Management Section","displayOrder":2,"__typename":"AppFunction"},
      {"afId":160,"parentAfId":155,"name":"[ Action: Remove ]","description":"Admin person can remove Business Rule on Management Section","displayOrder":3,"__typename":"AppFunction"},
      {"afId":161,"parentAfId":135,"name":"[ Action: Create ]","description":"Admin person can create Workflow","displayOrder":1,"__typename":"AppFunction"},
      {"afId":162,"parentAfId":135,"name":"[ Action: Enable ]","description":"Admin person can enable Workflow","displayOrder":2,"__typename":"AppFunction"},
      {"afId":163,"parentAfId":135,"name":"[ Action: Disable ]","description":"Admin person can disable Workflow","displayOrder":3,"__typename":"AppFunction"},
      {"afId":164,"parentAfId":135,"name":"Workflow Details","description":"Admin person has access to Workflow Detail","displayOrder":4,"__typename":"AppFunction"},
      {"afId":165,"parentAfId":164,"name":"Workflow Summary","description":"Admin person has access to Summary Section on Detail Workflow","displayOrder":1,"__typename":"AppFunction"},
      {"afId":166,"parentAfId":164,"name":"Workflow Management","description":"Admin person has access to Management Section on Detail Workflow","displayOrder":2,"__typename":"AppFunction"},
      {"afId":167,"parentAfId":165,"name":"[ Action: Edit ]","description":"Admin person can edit on Workflow Summary Section","displayOrder":1,"__typename":"AppFunction"},
      {"afId":168,"parentAfId":165,"name":"[ Action: Status Update ]","description":"Admin person can change status on Workflow Summary Section","displayOrder":2,"__typename":"AppFunction"},
      {"afId":169,"parentAfId":166,"name":"[ Action: Create ]","description":"Admin person can create Workflow on Management Section","displayOrder":1,"__typename":"AppFunction"},
      {"afId":170,"parentAfId":166,"name":"[ Action: Edit ]","description":"Admin person can edit Workflow on Management Section","displayOrder":2,"__typename":"AppFunction"},
      {"afId":171,"parentAfId":166,"name":"[ Action: Remove ]","description":"Admin person can remove Workflow on Management Section","displayOrder":3,"__typename":"AppFunction"},
      {"afId":172,"parentAfId":137,"name":"[ Action: Create ]","description":"Admin person can create new Service API","displayOrder":1,"__typename":"AppFunction"},
      {"afId":173,"parentAfId":137,"name":"[ Action: Enable ]","description":"Admin person can enable Service API","displayOrder":2,"__typename":"AppFunction"},
      {"afId":174,"parentAfId":137,"name":"[ Action: Disable ]","description":"Admin person can disable Service API","displayOrder":3,"__typename":"AppFunction"},
      {"afId":175,"parentAfId":137,"name":"Encryption, Access Information, Distribution List","description":"Admin person has access to Certificates-Access Information-Distribution List","displayOrder":4,"__typename":"AppFunction"},
      {"afId":176,"parentAfId":137,"name":"Service API Details","description":"Admin person has access to Service API Detail","displayOrder":5,"__typename":"AppFunction"},
      {"afId":177,"parentAfId":175,"name":"[ Action: Create Encryption ]","description":"Admin person can create new Encryption ","displayOrder":1,"__typename":"AppFunction"},
      {"afId":178,"parentAfId":175,"name":"[ Action: Create Access Information ]","description":"Admin person can create new Access Information","displayOrder":2,"__typename":"AppFunction"},
      {"afId":179,"parentAfId":175,"name":"[ Action: Create Distribution List ]","description":"Admin person can create new Distribution List","displayOrder":3,"__typename":"AppFunction"},
      {"afId":180,"parentAfId":175,"name":"Encryption Details","description":"Admin person has access to Encryption Detail","displayOrder":4,"__typename":"AppFunction"},
      {"afId":181,"parentAfId":175,"name":"Access Information Details","description":"Admin person has access to Access Information Detail","displayOrder":5,"__typename":"AppFunction"},
      {"afId":182,"parentAfId":175,"name":"Distribution List Details","description":"Admin person has access to Distribution List Detail","displayOrder":6,"__typename":"AppFunction"},
      {"afId":183,"parentAfId":180,"name":"Encryption Management","description":"Admin person has access to Management Section on Detail Encryption","displayOrder":1,"__typename":"AppFunction"},
      {"afId":184,"parentAfId":183,"name":"[ Action: Create ]","description":"Admin person can create Encryption on Management Section","displayOrder":1,"__typename":"AppFunction"},
      {"afId":185,"parentAfId":183,"name":"[ Action: Edit ]","description":"Admin person can edit Encryption on Management Section","displayOrder":2,"__typename":"AppFunction"},
      {"afId":186,"parentAfId":183,"name":"[ Action: Remove ]","description":"Admin person can remove Encryption on Management Section","displayOrder":3,"__typename":"AppFunction"},
      {"afId":187,"parentAfId":181,"name":"Access Information Management","description":"Admin person has access to Management Section on Detail Access Information","displayOrder":1,"__typename":"AppFunction"},
      {"afId":188,"parentAfId":187,"name":"[ Action: Create ]","description":"Admin person can create Access Information on Management Section","displayOrder":1,"__typename":"AppFunction"},
      {"afId":189,"parentAfId":187,"name":"[ Action: Edit ]","description":"Admin person can edit Access Information on Management Section","displayOrder":2,"__typename":"AppFunction"},
      {"afId":190,"parentAfId":187,"name":"[ Action: Remove ]","description":"Admin person can remove Access Information on Management Section","displayOrder":3,"__typename":"AppFunction"},
      {"afId":191,"parentAfId":182,"name":"Distribution List Management","description":"Admin person has access to Management Section on Detail Distribution List","displayOrder":1,"__typename":"AppFunction"},
      {"afId":192,"parentAfId":191,"name":"[ Action: Create ]","description":"Admin person can create Distribution List on Management Section","displayOrder":1,"__typename":"AppFunction"},
      {"afId":193,"parentAfId":191,"name":"[ Action: Edit ]","description":"Admin person can edit Distribution List on Management Section","displayOrder":2,"__typename":"AppFunction"},
      {"afId":194,"parentAfId":191,"name":"[ Action: Remove ]","description":"Admin person can remove Distribution List on Management Section","displayOrder":3,"__typename":"AppFunction"},
      {"afId":195,"parentAfId":176,"name":"Service API Summary","description":"Admin person has access to Summary Section on Detail Service API","displayOrder":1,"__typename":"AppFunction"},
      {"afId":196,"parentAfId":176,"name":"Service API Management","description":"Admin person has access to Management Section on Detail Service API","displayOrder":2,"__typename":"AppFunction"},
      {"afId":197,"parentAfId":195,"name":"[ Action: Edit ]","description":"Admin person can edit on Service API Summary Section","displayOrder":1,"__typename":"AppFunction"},
      {"afId":198,"parentAfId":195,"name":"[ Action: Status Update ]","description":"Admin person can change status on Service API Summary Section","displayOrder":2,"__typename":"AppFunction"},
      {"afId":199,"parentAfId":196,"name":"[ Action: Create ]","description":"Admin person can create Service API on Management Section","displayOrder":1,"__typename":"AppFunction"},
      {"afId":200,"parentAfId":196,"name":"[ Action: Edit ]","description":"Admin person can edit Service API on Management Section","displayOrder":2,"__typename":"AppFunction"},
      {"afId":201,"parentAfId":196,"name":"[ Action: Remove ]","description":"Admin person can remove Service API on Management Section","displayOrder":3,"__typename":"AppFunction"},
      {"afId":202,"parentAfId":196,"name":"[ Action: Link/Unlink ]","description":"Admin person can link or unlink Information from Service API on Management Section","displayOrder":4,"__typename":"AppFunction"},
      {"afId":203,"parentAfId":127,"name":"Cancel Support Task","description":"Allow Agent to Cancel a Support Task","displayOrder":6,"__typename":"AppFunction"},
      {"afId":204,"parentAfId":126,"name":"Mass Jobs Viewer","description":"Access to the mass jobs viewer within FSM Fulfillment.","displayOrder":6,"__typename":"AppFunction"},
      {"afId":206,"parentAfId":130,"name":"Catalogue","description":"Give Access to BRE-Catalogue tab","displayOrder":6,"__typename":"AppFunction"},
      {"afId":207,"parentAfId":206,"name":"Domain - Object","description":"Admin person has access to Domain and Object List","displayOrder":1,"__typename":"AppFunction"},
      {"afId":208,"parentAfId":207,"name":"[ Action: Create ]","description":"Allows you to create Domains and Objects","displayOrder":1,"__typename":"AppFunction"},
      {"afId":209,"parentAfId":207,"name":"Detail Screen","description":"To click on list item and display detail info","displayOrder":2,"__typename":"AppFunction"},
      {"afId":210,"parentAfId":209,"name":"[ Action: Create ]","description":"To create new object version and new properties (fields)","displayOrder":1,"__typename":"AppFunction"},
      {"afId":211,"parentAfId":209,"name":"[ Action: Edit ]","description":"To put the user in edit mode when looking at the objects details","displayOrder":2,"__typename":"AppFunction"},
      {"afId":212,"parentAfId":209,"name":"[ Action: Delete ]","description":"To remove objects and fields","displayOrder":3,"__typename":"AppFunction"},
      {"afId":213,"parentAfId":134,"name":"Business Rule Template","description":"Admin person has access to Business Rule Template","displayOrder":1,"__typename":"AppFunction"},
      {"afId":214,"parentAfId":213,"name":"[ Action: Create ]","description":"Admin person can create Business Rule Template","displayOrder":1,"__typename":"AppFunction"},
      {"afId":215,"parentAfId":213,"name":"[ Action: Enable ]","description":"Admin person can enable Business Rule Template","displayOrder":2,"__typename":"AppFunction"},
      {"afId":216,"parentAfId":213,"name":"[ Action: Disable ]","description":"Admin person can disable Business Rule Template","displayOrder":3,"__typename":"AppFunction"},
      {"afId":217,"parentAfId":213,"name":"Business Rule Template Details","description":"Admin person has access to Business Rule Template Detail","displayOrder":2,"__typename":"AppFunction"},
      {"afId":218,"parentAfId":217,"name":"Business Rule Template Summary","description":"Admin person has access to Summary Section on Detail Business Rule Template","displayOrder":1,"__typename":"AppFunction"},
      {"afId":219,"parentAfId":217,"name":"Business Rule Template Management","description":"Admin person has access to Management Section on Detail Business Rule Template","displayOrder":2,"__typename":"AppFunction"},
      {"afId":220,"parentAfId":218,"name":"[ Action: Edit ]","description":"Admin person can edit on Business Rule Template Summary Section","displayOrder":1,"__typename":"AppFunction"},
      {"afId":221,"parentAfId":218,"name":"[ Action: Status Update ]","description":"Admin person can change status on Business Rule Template Summary Section","displayOrder":2,"__typename":"AppFunction"},
      {"afId":222,"parentAfId":219,"name":"[ Action: Create ]","description":"Admin person can create Business Rule Template on Management Section","displayOrder":1,"__typename":"AppFunction"},
      {"afId":223,"parentAfId":219,"name":"[ Action: Edit ]","description":"Admin person can edit Business Rule Template on Management Section","displayOrder":2,"__typename":"AppFunction"},
      {"afId":224,"parentAfId":219,"name":"[ Action: Remove ]","description":"Admin person can remove Business Rule Template on Management Section","displayOrder":3,"__typename":"AppFunction"},
      {"afId":225,"parentAfId":104,"name":"Job Assignment Plan Management & Scheduler","description":"Allow the administrator to designate which users are allowed to create, modify and schedule Job Assignment Plans","displayOrder":6.1000000000000005,"__typename":"AppFunction"},
      {"afId":226,"parentAfId":104,"name":"Personnel Templates","description":"Allow the administrator to create Profile templates and assign them to non-technicians","displayOrder":7.1000000000000005,"__typename":"AppFunction"},
      {"afId":227,"parentAfId":127,"name":"Create Support Task","description":"Allow Agent to Create a Support Task","displayOrder":1,"__typename":"AppFunction"},
      {"afId":228,"parentAfId":127,"name":"Open Support Task","description":"Allow Agent to Open a Support Task","displayOrder":2,"__typename":"AppFunction"},
      {"afId":229,"parentAfId":127,"name":"Assign Support Task (Supervisor Only)","description":"Allow Agent to Assign a Support Task (Supervisor Only)","displayOrder":3,"__typename":"AppFunction"},
      {"afId":230,"parentAfId":127,"name":"Unassign Support Task","description":"Allow Agent to Unassign a Support Task","displayOrder":4,"__typename":"AppFunction"},
      {"afId":231,"parentAfId":127,"name":"Complete Support Task","description":"Allow Agent to Complete a Support Task","displayOrder":5,"__typename":"AppFunction"},
      {"afId":232,"parentAfId":127,"name":"Accept Support Task","description":"Allow Agent to Accept a Support Task","displayOrder":7,"__typename":"AppFunction"},
      {"afId":233,"parentAfId":127,"name":"Update Support Task Subject","description":"Allow Agent to Update a Support Task Subject","displayOrder":8,"__typename":"AppFunction"},
      {"afId":234,"parentAfId":128,"name":"Tech Information","description":"Allow Agent to Perform changes on Tech Information","displayOrder":1,"__typename":"AppFunction"},
      {"afId":235,"parentAfId":128,"name":"Tech Shift","description":"Allow Agent to Perform changes on Tech Shift","displayOrder":2,"__typename":"AppFunction"},
      {"afId":236,"parentAfId":128,"name":"Resource Configuration Management","description":"Allow Agent to Perform changes on Tech profile","displayOrder":3,"__typename":"AppFunction"},
      {"afId":237,"parentAfId":128,"name":"Modification","description":"Allow Agent to Perform changes on Modification","displayOrder":4,"__typename":"AppFunction"},
      {"afId":238,"parentAfId":234,"name":"Create Tech","description":"Allow Agent to Create New Tech","displayOrder":1,"__typename":"AppFunction"},
      {"afId":239,"parentAfId":234,"name":"Open Tech Details","description":"Allow Agent to Open Tech Details","displayOrder":2,"__typename":"AppFunction"},
      {"afId":240,"parentAfId":234,"name":"Expire Tech","description":"Allow Agent to Expire Tech","displayOrder":3,"__typename":"AppFunction"},
      {"afId":241,"parentAfId":234,"name":"Un-Expire Tech","description":"Allow Agent to Un-Expire Tech","displayOrder":4,"__typename":"AppFunction"},
      {"afId":242,"parentAfId":234,"name":"Edit Tech Personal Info","description":"Allow Agent to Edit Tech Personal Info","displayOrder":5,"__typename":"AppFunction"},
      {"afId":243,"parentAfId":234,"name":"Edit Tech Contact Info","description":"Allow Agent to Edit Tech Contact Info","displayOrder":6,"__typename":"AppFunction"},
      {"afId":244,"parentAfId":234,"name":"Reassign or Set Tech Supervisor","description":"Allow Agent to Reassign or Set Tech Supervisor","displayOrder":7,"__typename":"AppFunction"},
      {"afId":245,"parentAfId":234,"name":"Create Support Task","description":"Allow Agent to Create Support Task","displayOrder":8,"__typename":"AppFunction"},
      {"afId":246,"parentAfId":234,"name":"Send Message","description":"Allow Agent to Send Messages to a Tech","displayOrder":9,"__typename":"AppFunction"},
      {"afId":247,"parentAfId":235,"name":"Add Tech Shift","description":"Allow Agent to Add Tech Shift","displayOrder":1,"__typename":"AppFunction"},
      {"afId":248,"parentAfId":235,"name":"Edit Tech Shift","description":"Allow Agent to Edit Tech Shift","displayOrder":2,"__typename":"AppFunction"},
      {"afId":249,"parentAfId":235,"name":"Remove Tech Shift","description":"Allow Agent to Remove Tech Shift","displayOrder":3,"__typename":"AppFunction"},
      {"afId":250,"parentAfId":235,"name":"Add & Remove Tech Available Weeks","description":"Allow Agent to Add & Remove Tech Available Weeks","displayOrder":5,"__typename":"AppFunction"},
      {"afId":251,"parentAfId":235,"name":"Add & Remove Tech Available Days","description":"Allow Agent to Add & Remove Tech Available Days","displayOrder":6,"__typename":"AppFunction"},
      {"afId":252,"parentAfId":235,"name":"Copy Tech Shifts","description":"Allow Agent to Copy Tech Shifts","displayOrder":7,"__typename":"AppFunction"},
      {"afId":253,"parentAfId":235,"name":"Shift Availability","description":"Allow Agent to Perform changes on Shift Availability","displayOrder":8,"__typename":"AppFunction"},
      {"afId":254,"parentAfId":235,"name":"Shift Events","description":"Allow Agent to Perform changes on Shift Events","displayOrder":9,"__typename":"AppFunction"},
      {"afId":255,"parentAfId":235,"name":"Shift Service Areas","description":"Allow Agent to Perform changes on Shift Service Areas","displayOrder":10,"__typename":"AppFunction"},
      {"afId":256,"parentAfId":235,"name":"Shift Skill Groups","description":"Allow Agent to Perform changes on Shift Skill Groups","displayOrder":11,"__typename":"AppFunction"},
      {"afId":257,"parentAfId":235,"name":"Tech Association","description":"Allow Agent to Perform changes on Tech Association","displayOrder":12,"__typename":"AppFunction"},
      {"afId":258,"parentAfId":235,"name":"Modify Capacity Plan","description":"Allow Agent to Perform changes on Modify Capacity Plan","displayOrder":13,"__typename":"AppFunction"},
      {"afId":259,"parentAfId":236,"name":"Auto Dispatch Settings","description":"Allow Agent to Perform changes on Auto Dispatch Settings","displayOrder":1,"__typename":"AppFunction"},
      {"afId":260,"parentAfId":236,"name":"TechNet Settings","description":"Allow Agent to Perform changes on TechNet Settings","displayOrder":2,"__typename":"AppFunction"},
      {"afId":261,"parentAfId":236,"name":"Device Details","description":"Allow Agent to Perform changes on Device Details","displayOrder":3,"__typename":"AppFunction"},
      {"afId":262,"parentAfId":236,"name":"Skill Groups","description":"Allow Agent to Perform changes on Skill Groups","displayOrder":4,"__typename":"AppFunction"},
      {"afId":263,"parentAfId":236,"name":"Specialty Skills","description":"Allow Agent to Perform changes on Specialty Skills","displayOrder":5,"__typename":"AppFunction"},
      {"afId":264,"parentAfId":236,"name":"Licenses","description":"Allow Agent to Perform changes on Licenses","displayOrder":6,"__typename":"AppFunction"},
      {"afId":265,"parentAfId":236,"name":"Area Management","description":"Allow Agent to Perform changes on Area Management","displayOrder":7,"__typename":"AppFunction"},
      {"afId":266,"parentAfId":236,"name":"Preferred Starting Location","description":"Allow Agent to Perform changes on Preferred Starting Location","displayOrder":8,"__typename":"AppFunction"},
      {"afId":267,"parentAfId":237,"name":"Add Time Off","description":"Allow Agent to Add Time Off","displayOrder":1,"__typename":"AppFunction"},
      {"afId":268,"parentAfId":237,"name":"Remove Time Off","description":"Allow Agent to Remove Time Off","displayOrder":2,"__typename":"AppFunction"},
      {"afId":269,"parentAfId":237,"name":"Add Overtime","description":"Allow Agent to Add Overtime","displayOrder":3,"__typename":"AppFunction"},
      {"afId":270,"parentAfId":253,"name":"Add Tech Shift Availability","description":"Allow Agent to Add Tech Shift Availability","displayOrder":1,"__typename":"AppFunction"},
      {"afId":271,"parentAfId":253,"name":"Edit Tech Shift Availability","description":"Allow Agent to Edit Tech Shift Availability","displayOrder":2,"__typename":"AppFunction"},
      {"afId":272,"parentAfId":253,"name":"Remove Tech Shift Availability","description":"Allow Agent to Remove Tech Shift Availability","displayOrder":3,"__typename":"AppFunction"},
      {"afId":273,"parentAfId":254,"name":"Add Tech Shift events","description":"Allow Agent to Add Tech Shift events","displayOrder":1,"__typename":"AppFunction"},
      {"afId":274,"parentAfId":254,"name":"Edit Tech Shift events","description":"Allow Agent to Edit Tech Shift events","displayOrder":2,"__typename":"AppFunction"},
      {"afId":275,"parentAfId":254,"name":"Remove Tech Shift events","description":"Allow Agent to Remove Tech Shift events","displayOrder":3,"__typename":"AppFunction"},
      {"afId":276,"parentAfId":255,"name":"Add Tech Shift Service Areas","description":"Allow Agent to Add Tech Shift Service Areas","displayOrder":1,"__typename":"AppFunction"},
      {"afId":277,"parentAfId":255,"name":"Edit Tech Shift Service Areas","description":"Allow Agent to Edit Tech Shift Service Areas","displayOrder":2,"__typename":"AppFunction"},
      {"afId":278,"parentAfId":255,"name":"Remove Tech Shift Service Areas","description":"Allow Agent to Remove Tech Shift Service Areas","displayOrder":3,"__typename":"AppFunction"},
      {"afId":279,"parentAfId":256,"name":"Add Tech Shift Skill Group","description":"Allow Agent to Add Tech Shift Skill Group","displayOrder":1,"__typename":"AppFunction"},
      {"afId":280,"parentAfId":256,"name":"Edit Tech Shift Skill Group","description":"Allow Agent to Edit Tech Shift Skill Group","displayOrder":2,"__typename":"AppFunction"},
      {"afId":281,"parentAfId":256,"name":"Remove Tech Shift Skill Group","description":"Allow Agent to Remove Tech Shift Skill Group","displayOrder":3,"__typename":"AppFunction"},
      {"afId":282,"parentAfId":257,"name":"Add Tech Association","description":"Allow Agent to Add Tech Association","displayOrder":1,"__typename":"AppFunction"},
      {"afId":283,"parentAfId":257,"name":"Edit Tech Association","description":"Allow Agent to Edit Tech Association","displayOrder":2,"__typename":"AppFunction"},
      {"afId":284,"parentAfId":257,"name":"Remove Tech Association","description":"Allow Agent to Remove Tech Association","displayOrder":3,"__typename":"AppFunction"},
      {"afId":285,"parentAfId":257,"name":"Resolve Tech Association Conflicts","description":"Allow Agent to Resolve Tech Association Conflicts","displayOrder":4,"__typename":"AppFunction"},
      {"afId":286,"parentAfId":258,"name":"Overwrite Or Remove Capacity Model","description":"Allow Agent to Overwrite Or Remove Capacity Model","displayOrder":1,"__typename":"AppFunction"},
      {"afId":287,"parentAfId":258,"name":"Overwrite Or Remove Capacity Area","description":"Allow Agent to Overwrite Or Remove Capacity Area","displayOrder":2,"__typename":"AppFunction"},
      {"afId":288,"parentAfId":259,"name":"Enable & Disable Auto Dispatch","description":"Allow Agent to Enable & Disable Auto Dispatch","displayOrder":1,"__typename":"AppFunction"},
      {"afId":289,"parentAfId":259,"name":"Enable & Disable Adjust First Job EST","description":"Allow Agent to Enable & Disable Adjust First Job EST","displayOrder":2,"__typename":"AppFunction"},
      {"afId":290,"parentAfId":259,"name":"Enable & Disable Equipment Evaluation","description":"Allow Agent to Enable & Disable Equipment Evaluation","displayOrder":3,"__typename":"AppFunction"},
      {"afId":291,"parentAfId":260,"name":"Enable & Disable Acknowledge Jobs","description":"Allow Agent to Enable & Disable Acknowledge Jobs","displayOrder":1,"__typename":"AppFunction"},
      {"afId":292,"parentAfId":260,"name":"Enable & Disable Display Past Jobs","description":"Allow Agent to Enable & Disable Display Past Jobs","displayOrder":2,"__typename":"AppFunction"},
      {"afId":293,"parentAfId":260,"name":"Enable & Disable Force Job Order","description":"Allow Agent to Enable & Disable Force Job Order","displayOrder":3,"__typename":"AppFunction"},
      {"afId":294,"parentAfId":260,"name":"Set Number of Viewable Jobs","description":"Allow Agent to Set Number of Viewable Jobs","displayOrder":4,"__typename":"AppFunction"},
      {"afId":295,"parentAfId":261,"name":"Set Device Type","description":"Allow Agent to Set Device Type","displayOrder":1,"__typename":"AppFunction"},
      {"afId":296,"parentAfId":261,"name":"Enable & Disable Signature Capture","description":"Allow Agent to Enable & Disable Signature Capture","displayOrder":2,"__typename":"AppFunction"},
      {"afId":297,"parentAfId":262,"name":"Add Skill Groups","description":"Allow Agent to Add Skill Groups","displayOrder":1,"__typename":"AppFunction"},
      {"afId":298,"parentAfId":262,"name":"Edit Skill Groups","description":"Allow Agent to Edit Skill Groups","displayOrder":2,"__typename":"AppFunction"},
      {"afId":299,"parentAfId":262,"name":"Remove Skill Groups","description":"Allow Agent to Remove Skill Groups","displayOrder":3,"__typename":"AppFunction"},
      {"afId":300,"parentAfId":263,"name":"Add Specialty Skills","description":"Allow Agent to Add Specialty Skills","displayOrder":1,"__typename":"AppFunction"},
      {"afId":301,"parentAfId":263,"name":"Remove Specialty Skills","description":"Allow Agent to Remove Specialty Skills","displayOrder":2,"__typename":"AppFunction"},
      {"afId":302,"parentAfId":264,"name":"Add Licenses","description":"Allow Agent to Add Licenses","displayOrder":1,"__typename":"AppFunction"},
      {"afId":303,"parentAfId":264,"name":"Edit Licenses","description":"Allow Agent to Edit Licenses","displayOrder":2,"__typename":"AppFunction"},
      {"afId":304,"parentAfId":264,"name":"Remove Licenses","description":"Allow Agent to Remove Licenses","displayOrder":3,"__typename":"AppFunction"},
      {"afId":305,"parentAfId":265,"name":"Add Area Management","description":"Allow Agent to Add Area Management","displayOrder":1,"__typename":"AppFunction"},
      {"afId":306,"parentAfId":265,"name":"Edit Area Management","description":"Allow Agent to Edit Area Management","displayOrder":2,"__typename":"AppFunction"},
      {"afId":307,"parentAfId":265,"name":"Remove Area Management","description":"Allow Agent to Remove Area Management","displayOrder":3,"__typename":"AppFunction"},
      {"afId":308,"parentAfId":266,"name":"Add Preferred Starting Location","description":"Allow Agent to Add Preferred Starting Location","displayOrder":1,"__typename":"AppFunction"},
      {"afId":309,"parentAfId":266,"name":"Remove Preferred Starting Location","description":"Allow Agent to Remove Preferred Starting Location","displayOrder":2,"__typename":"AppFunction"},
      {"afId":310,"parentAfId":235,"name":"Set Tech Shift Rotation","description":"Allow Agent to Set Tech Shift Rotation","displayOrder":4,"__typename":"AppFunction"},
      {"afId":311,"parentAfId":126,"name":"Tech Actions","description":"Allow Agent to Perform Tech Actions","displayOrder":1,"__typename":"AppFunction"},
      {"afId":312,"parentAfId":126,"name":"Tech Shift","description":"Allow Agent to Perform changes on Tech shift","displayOrder":2,"__typename":"AppFunction"},
      {"afId":313,"parentAfId":126,"name":"Tech Modification","description":"Allow Agent to Perform changes on Modification","displayOrder":3,"__typename":"AppFunction"},
      {"afId":314,"parentAfId":126,"name":"Assign & Unassign Job","description":"Allow Agent to Assign and Unassign Jobs","displayOrder":4,"__typename":"AppFunction"},
      {"afId":315,"parentAfId":126,"name":"Job Details","description":"Allow Agent to Perform changes on Job Details","displayOrder":5,"__typename":"AppFunction"},
      {"afId":316,"parentAfId":126,"name":"Assignment Plan","description":"Allow Agent to Perform changes on Assignment Plan","displayOrder":7,"__typename":"AppFunction"},
      {"afId":317,"parentAfId":126,"name":"Unordered Work","description":"Allow Agent to Perform changes on Unordered Work","displayOrder":8,"__typename":"AppFunction"},
      {"afId":318,"parentAfId":311,"name":"Open Tech Details","description":"Allow Agent to Open Tech Details","displayOrder":1,"__typename":"AppFunction"},
      {"afId":319,"parentAfId":311,"name":"Create Support Task","description":"Allow Agent to Create Support Task","displayOrder":2,"__typename":"AppFunction"},
      {"afId":320,"parentAfId":311,"name":"Send Message","description":"Allow Agent to Send Messages to a Tech","displayOrder":3,"__typename":"AppFunction"},
      {"afId":321,"parentAfId":311,"name":"Auto Assignment","description":"Allow Agent to Run and Apply Auto Assignment","displayOrder":4,"__typename":"AppFunction"},
      {"afId":322,"parentAfId":311,"name":"Job Order Route","description":"Allow Agent to Run Job Order Route","displayOrder":5,"__typename":"AppFunction"},
      {"afId":323,"parentAfId":311,"name":"Best Jobs","description":"Allow Agent to Find and Apply Best Jobs","displayOrder":6,"__typename":"AppFunction"},
      {"afId":324,"parentAfId":312,"name":"Add Tech Shift Events","description":"Allow Agent to Add Tech shift events","displayOrder":1,"__typename":"AppFunction"},
      {"afId":325,"parentAfId":312,"name":"Tech Shift Service Areas","description":"Allow Agent to Perform changes on Tech Shift Service Areas","displayOrder":2,"__typename":"AppFunction"},
      {"afId":326,"parentAfId":312,"name":"Tech Shift Skill Groups","description":"Allow Agent to Perform changes on Tech Shift Skill Groups","displayOrder":3,"__typename":"AppFunction"},
      {"afId":327,"parentAfId":312,"name":"Modify Capacity Plan","description":"Allow Agent to Perform changes on Modify Capacity Plan","displayOrder":4,"__typename":"AppFunction"},
      {"afId":328,"parentAfId":313,"name":"Add Time Off Request","description":"Allow Agent to Add Time Off","displayOrder":1,"__typename":"AppFunction"},
      {"afId":329,"parentAfId":313,"name":"Remove Time Off Request","description":"Allow Agent to Remove Time Off","displayOrder":2,"__typename":"AppFunction"},
      {"afId":330,"parentAfId":313,"name":"Add Overtime","description":"Allow Agent to Add Overtime","displayOrder":3,"__typename":"AppFunction"},
      {"afId":331,"parentAfId":315,"name":"Change Dispatch Operations Status","description":"Allow Agent to Change Dispatch Operations Status","displayOrder":1,"__typename":"AppFunction"},
      {"afId":332,"parentAfId":315,"name":"Start Job","description":"Allow Agent to Start Job","displayOrder":2,"__typename":"AppFunction"},
      {"afId":333,"parentAfId":315,"name":"Stop Job","description":"Allow Agent to Stop Job","displayOrder":3,"__typename":"AppFunction"},
      {"afId":334,"parentAfId":315,"name":"Recalculate Latitude & Longitude","description":"Allow Agent to Recalculate Latitude & Longitude","displayOrder":4,"__typename":"AppFunction"},
      {"afId":335,"parentAfId":315,"name":"Add/Update/Remove Job Activities or Subactivities","description":"Allow Agent to Add/Update/Remove Job Activities or Subactivities","displayOrder":5,"__typename":"AppFunction"},
      {"afId":336,"parentAfId":315,"name":"Complete Job","description":"Allow Agent to Complete Job","displayOrder":6,"__typename":"AppFunction"},
      {"afId":337,"parentAfId":315,"name":"Cancel Job","description":"Allow Agent to Cancel Job","displayOrder":7,"__typename":"AppFunction"},
      {"afId":338,"parentAfId":315,"name":"Set Job in Held/Hold Status","description":"Allow Agent to Set Job in Held/Hold Status","displayOrder":8,"__typename":"AppFunction"},
      {"afId":339,"parentAfId":315,"name":"Add Job Comment","description":"Allow Agent to Add Job Comment","displayOrder":9,"__typename":"AppFunction"},
      {"afId":340,"parentAfId":315,"name":"Project Details","description":"Allow Agent to Perform changes on Project Details","displayOrder":10,"__typename":"AppFunction"},
      {"afId":341,"parentAfId":204,"name":"Export Mass Jobs Viewer Results","description":"Allow Agent to Export Mass Jobs Viewer Results","displayOrder":1,"__typename":"AppFunction"},
      {"afId":342,"parentAfId":316,"name":"Run Assignment Plan","description":"Allow Agent to Run Assignment Plan","displayOrder":1,"__typename":"AppFunction"},
      {"afId":343,"parentAfId":342,"name":"Save Assignment Plan","description":"Allow Agent to Save Assignment Plan","displayOrder":1,"__typename":"AppFunction"},
      {"afId":344,"parentAfId":317,"name":"Change Job EST","description":"Allow Agent to Change Job EST","displayOrder":1,"__typename":"AppFunction"},
      {"afId":345,"parentAfId":317,"name":"Change Tech Shift Event Start Time","description":"Allow Agent to Change Tech Shift Event Start Time","displayOrder":2,"__typename":"AppFunction"},
      {"afId":346,"parentAfId":325,"name":"Add Tech Shift Service Areas","description":"Allow Agent to Add Tech Shift Service Areas","displayOrder":1,"__typename":"AppFunction"},
      {"afId":347,"parentAfId":325,"name":"Remove Tech Shift Service Areas","description":"Allow Agent to Remove Tech Shift Service Areas","displayOrder":2,"__typename":"AppFunction"},
      {"afId":348,"parentAfId":326,"name":"Add Tech Shift Skill Group","description":"Allow Agent to Add Tech Shift Skill Group","displayOrder":1,"__typename":"AppFunction"},
      {"afId":349,"parentAfId":326,"name":"Remove Tech Shift Skill Group","description":"Allow Agent to Remove Tech Shift Skill Group","displayOrder":2,"__typename":"AppFunction"},
      {"afId":350,"parentAfId":327,"name":"Overwrite Or Remove Capacity Model","description":"Allow Agent to Overwrite Or Remove Capacity Model","displayOrder":1,"__typename":"AppFunction"},
      {"afId":351,"parentAfId":327,"name":"Overwrite Or Remove Capacity Area","description":"Allow Agent to Overwrite Or Remove Capacity Area","displayOrder":2,"__typename":"AppFunction"},
      {"afId":352,"parentAfId":340,"name":"Create Associated Job","description":"Allow Agent to Create an Associated Job","displayOrder":1,"__typename":"AppFunction"},
      {"afId":353,"parentAfId":340,"name":"Complete Project","description":"Allow Agent to Complete Project","displayOrder":2,"__typename":"AppFunction"},
      {"afId":354,"parentAfId":340,"name":"Cancel Project","description":"Allow Agent to Cancel Project","displayOrder":3,"__typename":"AppFunction"},
      {"afId":355,"parentAfId":125,"name":"Scheduling","description":"Allow the administrator to designate which users are allowed to launch Scheduling from the Web UI","displayOrder":4,"__typename":"AppFunction"},
      {"afId":356,"parentAfId":101,"name":"User Management","description":"Give Access to User Management","displayOrder":2,"__typename":"AppFunction"},
      {"afId":357,"parentAfId":125,"name":"Map Analytics","description":"Allow Agent to view Map Analytics","displayOrder":2,"__typename":"AppFunction"},
      {"afId":358,"parentAfId":357,"name":"Boundary Configurator","description":"Allow Agent to configure boundaries in Map Analytics","displayOrder":1,"__typename":"AppFunction"},
      {"afId":359,"parentAfId":315,"name":"Allow Schedule Change To Non-sched Jobs","description":"Allow user to change the schedule date of a job with available capacity timeslots","displayOrder":11,"__typename":"AppFunction"},
      {"afId":360,"parentAfId":359,"name":"Allow Force Booking","description":"Allow user to select a non-available capacity timeslot and force book it","displayOrder":1,"__typename":"AppFunction"},
      {"afId":361,"parentAfId":315,"name":"Add/Remove Tags to Jobs","description":"Allow Agent to Add/Remove Tags to Jobs","displayOrder":12,"__typename":"AppFunction"}
   ]}}

####################################
def setup_wexis(emu):
    emu.set_response({"rp":"EntWexis::wp_wexis_login", "path":"1.0/login"}, get_wexis_login)
    emu.set_response({"path":"1.0/personnel/39099999"}, get_wexis_personnel)
    emu.set_response({"path":"1.0/personnel"}, get_wexis_personnel)
    emu.set_response({"path":"1.0/dispositions/config"}, {"isRequired":false,"dispositionDefinitions":[]})
    emu.set_response({"path":"1.1/inisettings"}, get_wexis_inisettings)
    emu.set_response({"path":"1.0/slas"}, get_wexis_slas)
    emu.set_response({"path":"1.0/personnelstatuses/39099999"}, get_wexis_personnelstatuses),
    emu.set_response({"path":"1.2/me/filters"}, get_wexis_me_filters)
    emu.set_response({"rp":"EntWexis::wp_wexis_unk", "path":"1.0/login"}, get_wexis_me_filters)

def get_wexis_login():
    return {"access_token":"xSS+TVk3C553n43NEvUZhnEo6fgLP0U5cdgRdbO9LFI=","refresh_token":"lAErCTiu87ZnYHjAMae5vWQjnc0OcIuwJ4WNBUQS4/k=","expires_in":3600,"token_type":"bearer","scope":[{"name":"Fulfillment","afId":126},{"name":"Support Tasks","afId":127},{"name":"Resource Allocation","afId":128},{"name":"Settings & Configuration","afId":130},{"name":"Normalization","afId":133},{"name":"Business Rule","afId":134},{"name":"Workflow","afId":135},{"name":"Event Management","afId":136},{"name":"Service Api","afId":137},{"name":"Cancel Support Task","afId":203},{"name":"Mass Jobs Viewer","afId":204},{"name":"Catalogue","afId":206},{"name":"Create Support Task","afId":227},{"name":"Open Support Task","afId":228},{"name":"Assign Support Task (Supervisor Only)","afId":229},{"name":"Unassign Support Task","afId":230},{"name":"Complete Support Task","afId":231},{"name":"Accept Support Task","afId":232},{"name":"Update Support Task Subject","afId":233},{"name":"Tech Information","afId":234},{"name":"Tech Shift","afId":235},{"name":"Resource Configuration Management","afId":236},{"name":"Modification","afId":237},{"name":"Tech Actions","afId":311},{"name":"Tech Shift","afId":312},{"name":"Tech Modification","afId":313},{"name":"Assign & Unassign Job","afId":314},{"name":"Job Details","afId":315},{"name":"Assignment Plan","afId":316},{"name":"Unordered Work","afId":317},{"name":"Scheduling","afId":355},{"name":"Map Analytics","afId":357},{"name":"Boundary Configurator","afId":358}],"reset_password":"N"}

def get_wexis_personnel():
    return {
        "FirstName":"David",
        "LastName":"Totschnig",
        "PersonnelId":39099999,
        "CompanyId":null,
        "CompanyName":null,
        "IsSupervisor":true,
        "Status":"Unknown",
        "ResetPassword":"N",
        "BuPrsnIdsMapping":{"3900":39099999,"3901":39099999,"3902":39099999,"3903":39099999}
    };

def get_wexis_inisettings():
    return {
        "MSO_SUPPORT_TASK_REQUIRE_DISPOSITION":"ON",
        "MSO_SUPPORT_TASK_EXCEPTION":"OFF",
        "KAIROS_EXPERIMENTAL":null,
        "MSO_KAIROS_POLLING_INTERVAL":null,
        "SCHEDULING_EXPERIMENTAL":null,
        "MSO_MJV_DAYS_DISPLAY_NUM":"1",
        "MSO_JOB_ASSIGNMENT_PLAN":"OFF",
        "MSO_KAIROS_SCHEDULING_URL":null,
        "MSO_KAIROS_CORE_APPS_URL":null,
        "MSO_FSM_UI_SHOW_JOB_SPECIALTY_SKILLS":"OFF",
        "BING_API_KEY":"AlQXn1prpYPuUS2WFqCurb4GsT5suryQ32hSodyIyNsFU6tuxLiD02__E_aGxVwT",
        "MSO_SCX_BASE_URL":null,
        "MSO_SCX_TOKEN":null,
        "FC_SCHEDULING_CAPACITY_SOURCE":null,
        "MSO_USE_SRS_API":null
    }

def get_wexis_slas():
    return {"List":[
        {"SLAName":"SLA1","SLATypeCd":"TASKS","CompleteRatePercentage":{"Warning":90,"Alert":70},
         "MaxUnassignedMins":{"Warning":300,"Alert":360},
         "AvgUnassignedMins":{"Warning":300,"Alert":360},
         "MaxAssignedMins":{"Warning":2300,"Alert":3000},
         "AvgAssignedMins":{"Warning":300,"Alert":360},
         "MaxNonProductiveMins":60,
         "MaxNonProductiveUsers":100,
         "AddtionalUsersNeededNum":{"Warning":100,"Alert":100}
         },
        {"SLAName":"SLA1","SLATypeCd":"TASKS","CompleteRatePercentage":{"Warning":90,"Alert":70},
         "MaxUnassignedMins":{"Warning":300,"Alert":360},
         "AvgUnassignedMins":{"Warning":300,"Alert":360},
         "MaxAssignedMins":{"Warning":2300,"Alert":3000},
         "AvgAssignedMins":{"Warning":300,"Alert":360},
         "MaxNonProductiveMins":60,
         "MaxNonProductiveUsers":100,
         "AddtionalUsersNeededNum":{"Warning":100,"Alert":100}
         }
    ]}

def get_wexis_personnelstatuses():
    return {"List":[{"status":"AVAIL","code":null,"display":"Y","autoassignment":"Y","isPersonnelAutoAssign":false},
                    {"status":"LOFF","code":null,"display":"Y","autoassignment":"N","isPersonnelAutoAssign":false}]}

def get_wexis_me_filters():
    return {"List":[{"TimeZone":"US Eastern","FilterId":"39000000","FulfillmentCenter":"3900","FilterName":"AOF0"}]}

