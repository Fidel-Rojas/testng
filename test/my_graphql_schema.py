import asyncio
import graphene
import logging
log = logging.getLogger('mymy')

class Query(graphene.ObjectType):
    hello = graphene.String()
    @staticmethod
    def resolve_hello(obj, info, **kwargs):
        log.info("GGG watchDispatchMessages")
        return "world"


# const DISPATCH_MESSAGE_FIELDS = `
#   msgId: Int!
#   readDate: String
#   owner: String
#   buId: Int
#   typeCode: String
# `;
# const typeDefs = gql`
#   type Query {
#     _emptyname: String
#   }

#   type DispatchMessageChange {
#     ${DISPATCH_MESSAGE_FIELDS}
#   }

#   type Subscription {
#     watchDispatchMessages(filters: [Int]!, fulfillmentCenterId: Int!, entPersonId: Int!, includeSystemMessages: Boolean): DispatchMessageChange
#   }
# `;

# query from browser:
# {id: "1", type: "start", payload: {,…}}
# id: "1"
# payload: {,…}
# extensions: {}
# operationName: "WatchDispatchMessages"
# query: "subscription WatchDispatchMessages($fulfillmentCenterId: Int!, $personId: Int!, $includeSystemMessages: Boolean, $filters: [Int]!) {
# messageUpdate: watchDispatchMessages(fulfillmentCenterId: $fulfillmentCenterId, entPersonId: $personId, includeSystemMessages: $includeSystemMessages, filters: $filters) {
#         msgId
#         readDate
#         owner
#         buId
#         typeCode
#         __typename
#       }
# }
# "
# variables: {personId: 39099999, fulfillmentCenterId: 3900, includeSystemMessages: false, filters: [-1]}
# type: "start"
        

class DispatchMessageChange(graphene.ObjectType):
    msgId=graphene.Int()
    readDate=graphene.String()
    owner=graphene.String()
    buId=graphene.Int()
    typeCode=graphene.String()

class WatchDispatchMessages(graphene.ObjectType):
    fulfillmentCenterId=graphene.Int()
    personId=graphene.Int()
    includeSystemMessages=graphene.Boolean()
    filters=graphene.List(graphene.Int)

class Subscription(graphene.ObjectType):
    watchDispatchMessages = graphene.Field(WatchDispatchMessages)
    async def subscribe_watchDispatchMessages(root, info):
        while True:
            yield msg

def yo():
    log.info("WTF is a graphene.String() ", graphene.String())
    
schema = graphene.Schema(query=Query, subscription=Subscription)
yo()
