import logging
import pytest
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
#from selenium.webdriver.support.ui import WebDriverWait
#from selenium.webdriver.support import expected_conditions as EC
#from selenium.webdriver.common.action_chains import ActionChains
import code
from seleniumbase import BaseCase
from fsm_gui_data_packets import Job, Tech, Tsh
import fake

log = logging.getLogger(__name__)

null = None
class MyTestClass(BaseCase):
   @pytest.fixture(autouse=True)
   def my_setup(self, setup, backend_emu):
       self.backend_emu = backend_emu
       num_jobs = 6000
       self.backend_emu.set_response("/jobs", self.get_jobs(num_jobs))
       self.backend_emu.set_response("/techs", self.get_techs(num_jobs=num_jobs))
       self.backend_emu.setup_job_updates(num_seconds=60, num_updates_per_second=1, get_job_fn=self.my_upd_job,
                                          start_after_seconds=10)
       
   def my_upd_job(self, i):
      job = self.my_job(i)
      job.job_state = 'CP'
      job.start_time = f"2023-03-01 {str(int(i%12+8)).zfill(2)}:00:00-05:00"
      job.end_time = f"2023-03-01 {str(int(i%12+8)).zfill(2)}:45:00-05:00"
      return job

   def my_job(self, i):
      job = fake.get_job(i, t=int(i/4))
      job.est = f"2023-03-01 {str(int(i%4+8)).zfill(2)}:00:00-05:00"
      job.duration_mins = 45
      if i < 900:
         job.oose_ids = [i*100]
         job.oose_errors = ['error-occurred']
      return job
      
   def get_jobs(self, num):
      rows = []
      for i in range(num):
         job = self.my_job(i)
         rows.append(job.data)
      return {"rows":rows, "columns":Job.fields}

   def get_techs(self, num_jobs):
      num = int(num_jobs/4) + 1
      rows = []
      for i in range(num):
         tech = fake.get_tech(i)
         rows.append(tech.data)
      return {"rows":rows, "columns":Tech.fields, "shiftColumns":Tsh.fields}

   def login(self):
      url = self.backend_emu.url_for(f"/ent/{self.mso}/fsm/login")
      log.info('url %s', url)
      self.set_window_size(2400, 1200)
      self.open(url)
      #code.interact(local=locals())
      self.type("#username", "J39")
      self.type("#password", "B")
      self.type("#password", Keys.RETURN)
      self.assert_text("Fulfillment", "div.label")
       
   def test_job_updates(self):
      self.login()
      self.click('div[routerlink="/fulfillment-v2"]')
      self.assert_text("dvsn3920", timeout=20)
      def expand_tree_element(title):
         self.get_element(f'div[title="{title}"]', timeout=1).find_element(By.XPATH, "..").find_element(By.XPATH, "..").find_element(By.CSS_SELECTOR, "a").click()
      def better_expand_tree_element(title):
         self.click(f'p-treetabletoggler[name="{title}"] .ui-treetable-toggler')
      expand_tree_element('dvsn3920')
      expand_tree_element('site3910')
      def click_tree_element(title):
         self.get_element(f'div[title="{title}"]', timeout=1).find_element(By.XPATH, "..").find_element(By.CLASS_NAME, "p-checkbox").click()
      def better_click_tree_element(title):
         self.click(f'p-treetablecheckbox[name="{title}"] .p-checkbox', timeout=1)
      click_tree_element('flctr3900')
      log.info("GGG")
      code.interact(local=locals())
      import time
      time.sleep(20)

