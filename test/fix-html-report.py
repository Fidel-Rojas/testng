import sys
import os
import pathlib

def main():
    path = sys.argv[1]
    assert path
    os.rename(f"{path}/webpack:", f"{path}/webpack")
    num_fixed = fix_html_files(path)
    print("fixed", num_fixed, "files")

def fix_html_files(fix_html_path):
    num = 0
    for fname in pathlib.Path(fix_html_path).rglob("*.html"):
        text = read_file(fname)
        text = text.replace("webpack:///", "webpack/")
        text = text.replace('="../../', '="')
        write_file(fname, text)
        num += 1
    return num

def write_file(fname, text):
   with open(fname, 'w') as f:
       f.write(text)

def read_file(fname):
    with open(fname, mode="r") as f:
        text = f.read()
    return text

main()
