from fsm_gui_data_packets \
   import Job, Tech, Tsh, TechGps, JobFilterData, TechStatus, FilterSession, JobResched, \
   SkillGroup, KeyValueMapping, CapacityCategory, Timeslot, TechFilteredOut, SupportTask

def setup_fields():
   job_cmp_fields = """work_order_number order_num job_number bu_id
   job_state work_order_type est estimated_points eta work_order_class
   working_area service_area dispatcher_status timeslot_code timeslot_id duration_mins
   start_time end_time company_id ordered
   tech_id tech_num tech_first_name tech_last_name
   project_job_code
   cust_name customer_type vip_cd
   street_name street_name_2 building apartment city state zip_code_plus
   latitude longitude specialty_skill_count
   skill_codes event_types oose_ids oose_errors tags skill_priority_override route_first_ind lob_node reason_codes assignment_type skill_prios
   """.split()
   job_timestamp_fields = ['entity_timestamp']
   Job.set_fields(job_cmp_fields, job_timestamp_fields)
   tech_cmp_fields = """tech_id tech_num company_id employee_flag
   latitude longitude pool_type tech_status_cd
   tech_first_name tech_last_name
   fsup_tech_id fsup_tech_first_name fsup_tech_last_name isa_fsup expiration_datetime
   title office_name
   isa_primary_tech isa_associated_tech has_association_conflict
   num_tschev_unordered time_offset_from_utc
   event_types tech_shifts
   """.split()
   tech_timestamp_fields = ['entity_timestamp', 'tech_status_datetime']
   Tech.set_fields(tech_cmp_fields, tech_timestamp_fields)
   tsh_cmp_fields = """break_begin_time break_end_time
   current_fulfill_center_bu_id daily_override_ind latitude longitude
   shift_begin_time shift_end_time shift_type_cd
   wcam_id work_day_ind wsrm_id
   routing_criterias skill_codes tech_shift_id
   """.split()
   Tsh.set_fields(tsh_cmp_fields)
   support_task_cmp_fields = """st_id job_number work_order_number std_id flctr_bu_id
   ent_prsn_id category subject body tech_id tech_first_name tech_last_name tech_num status
   create_dt accept_dt complete_dt new_message_cnt notes st_actn_type_cd
   """.split()
   SupportTask.set_fields(support_task_cmp_fields)
   JobFilterData.set_fields(
      """bu_id tech_id skill_codes working_area timeslot_id company_id""".split())
   TechGps.set_fields("""tech_id latitude longitude motion_status""".split(), ['entity_timestamp'])
   TechStatus.set_fields(['tech_id', 'tech_status_cd'], ['tech_status_datetime'])
   FilterSession.set_fields(['ent_filter_session_id']);
   JobResched.set_fields(['work_order_number'], ['entity_timestamp'])
   TechFilteredOut.set_fields(['tech_id'])
   SkillGroup.set_fields(['skill_code', 'skill_desc', 'skill_priority'])
   KeyValueMapping.set_fields(['key', 'value'])
   CapacityCategory.set_fields(['wsc_id', 'sched_category', 'wcam_id'])
   Timeslot.set_fields(['timeslot_id', 'timeslot_abbr', 'timeslot_desc', 'timeslot_begin_time', 'timeslot_end_time'])