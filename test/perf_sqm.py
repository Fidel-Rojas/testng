import logging
import code
import time
import random
from datetime import datetime
from unittest.mock import ANY
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import base_case
import fake
from fsm_gui_data_packets import SupportTask

log = logging.getLogger(__name__)
null = None

class SqmBase(base_case.BaseCaseSqmTab):

   def get_dispositions(self):
      return [39000050]
   
   def get_dispositions_config(self):
      log.info("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGg")
      return {
         "isRequired": True,
         "dispositionDefinitions": [
            {"id": 39000050, "name": "dispo-0-50", "parentId": null},
            {"id": 39000051, "name": "dispo-0-51", "parentId": null},
            {"id": 39000052, "name": "dispo-0-52", "parentId": null}
         ]}

   def get_resolutions(self):
      return [
         dict(
            taskId=39000000,
            resolutionId=101,
            resolutionText='resolution 1'),
         dict(
            taskId=39000000,
            resolutionId=102,
            resolutionText='resolution 2'),
      ]
      
   def get_subjects(self):
      return dict(List=[
         {"SupportTaskDefnId": 39000000,
          "Subject": "taskdefn-0-0",
          "Message": "taskdefn0 message",
          "DisplayOrder": 1,
          "SupportTaskCategoryDefnId": 39000000,
          "Category": "cat-0-0"},
         {"SupportTaskDefnId": 39000001,
          "Subject": "taskdefn-0-1",
          "Message": "taskdefn1 message",
          "DisplayOrder": 1,
          "SupportTaskCategoryDefnId": 39000001,
          "Category": "cat-0-1"},
      ])
      
   def get_messages(self):
      return [
         {"taskId":39000000,
          "messageId":null,
          "personnelId":39099999,
          "enterpriseFirstName":"David",
          "enterpriseLastName":"Totschnig",
          "techId":39000000,
          "submitDate":"2023/05/15 04:00:00.000",
          "readDate":"2023/05/15 13:24:33.920",
          "createdBy":"UNKNOWN",
          "message":"support task message here"
          },
         {"taskId":39000000,
          "messageId":469,
          "personnelId":39099999,
          "enterpriseFirstName":"David",
          "enterpriseLastName":"Totschnig",
          "techId":39000000,
          "submitDate":"2023/05/15 12:33:01.972",
          "readDate":null,
          "createdBy":"AGENT",
          "message":
          "support task message here David Totschnig has accepted this task."}]
   
   def get_techmessage_readdate(self):
      return {"taskId":39000000}

   def get_msgtasks(self):
      return {
         "Subject": "taskdefn-0-1",
         "Id": 39000001,
         "Type": "SUPPORT_TASK",
         "Sender": {
            "Type": "TECH",
            "Id": "T000",
            "IdType": "WFX_IDENTIFIER"
         },
         "MessageId": "2202",
         "Body": "taskdefn1 message",
         "FulfillmentCenter": 3900,}
      
   def get_graphql_get_techs_by_tech_ids(self):
      graphql_rows = []
      for tech in self.techs:
         graphql_rows.append({
            "techId":tech.tech_id,"techNum":tech.tech_num,
            "firstName":tech.tech_first_name,"lastName":tech.tech_last_name,"__typename":"Tech",
            "techPhoto":null,"status":tech.tech_status_cd,"fulfillmentCenterId":3900})
      return {"data":{"getTechsByTechIds":graphql_rows}}
   
   def get_graphql_get_stask_attachment(self):
      return {"data":{"result":{"attachments":[],"__typename":"SupportTask"}}}

   def get_graphql_tasks_resolution_codes(self):
      return {"data": {
         "defns": [
            {"definitionId": 39000000, "resolutions": [{"id": 39000000,"description": "Equipment resolved","__typename": "SupportTaskResolution"}],
             "__typename": "SupportTaskDefinitionResolution"
             },
            # {"definitionId": 39000001, "resolutions": [{"id": 39000001,"description": "Provisioned","__typename": "SupportTaskResolution"},
            #                                            {"id": 39000002,"description": "Cancel","__typename": "SupportTaskResolution"},],
            #  "__typename": "SupportTaskDefinitionResolution"
            #  },
         ]}}
         # {'definitionId': 643, 'resolutions':[{'id': 241, 'description': 'Complete', "__typename":"ResolutionCode"}], "__typename":"SupporTaskDefinition"},
         # {'definitionId': 644, 'resolutions':[{'id': 184, 'description': 'Equipment resolved', "__typename":"ResolutionCode"}], "__typename":"SupporTaskDefinition"},
         # {'definitionId': 645, 'resolutions':[{'id': 184, 'description': 'Equipment resolved', "__typename":"ResolutionCode"}], "__typename":"SupporTaskDefinition"},
         # {'definitionId': 646, 'resolutions':[{'id': 182, 'description': 'Provisioned', "__typename":"ResolutionCode"},
         #                                      {'id': 243, 'description': 'Cancel', "__typename":"ResolutionCode"}], "__typename":"SupporTaskDefinition"}
   
   def get_wexis_msgtasks_task_accept(self, st):
      log.info("in get_wexis_msgtasks_task_accept")
      self.task_accept_called = True
      st = self.to_support_task(st)
      return {
         "mso":"mso3930",
         "msoBuId":3930,
         "personnelId":st.ent_prsn_id,
         "supportTaskId":st.st_id,
         "fulfillmentCenter":st.flctr_bu_id,
         "techId":st.tech_id,
         "definitionId":st.std_id,
         "subject":st.subject,
         "message":st.body,
         "category":st.category,
         "workOrderNumber":st.work_order_number,
         "createDate":st.create_dt,
         "priority":2}

class TestSupportTaskInqueueLoad(SqmBase):

   def test_it(self):
      self.emu.set_response({'operationName': "GetTechsByTechIds"}, self.get_graphql_get_techs_by_tech_ids)
      num_techs = 500
      self.emu.set_response("/techs", fake.techs(num_techs))
      num_user_support_tasks = 50
      num_inqueue_support_tasks = 5000
      next_index = 0
      for n in range(0,num_user_support_tasks):
         self.add_support_task(next_index, ent_prsn_id=self.get_user_id(), status='WORKING' if next_index%3 else "COMPLETED", accept_dt=self.utcnow(),
                               tech=next_index%num_techs)
         next_index +=1
      for n in range(0,num_inqueue_support_tasks):
         self.add_support_task(next_index, status='INQUEUE', tech=next_index%num_techs)
         next_index +=1
      self.navigate_to_sqm()
      self.click_sidebar_support_tasks_button()
      code.interact(local=locals())

class TestSupportTaskRealTimeUpdatesLoad(SqmBase):

   def test_it(self):
      self.emu.set_response({'operationName': "GetTechsByTechIds"}, self.get_graphql_get_techs_by_tech_ids)
      num_techs = 500
      self.emu.set_response("/techs", fake.techs(num_techs))
      num_user_support_tasks = 50
      num_inqueue_support_tasks = 50
      next_index = 0
      for n in range(0,num_user_support_tasks):
         self.add_support_task(next_index, ent_prsn_id=self.get_user_id(), status='WORKING' if next_index%3 else "COMPLETED", accept_dt=self.utcnow(),
                               tech=next_index%num_techs)
         next_index +=1
      for n in range(0,num_inqueue_support_tasks):
         self.add_support_task(next_index, status='INQUEUE', tech=next_index%num_techs)
         next_index +=1
      self.navigate_to_fm_flctr3900()
      freq = dict(start_after_seconds=10, num_seconds=60, num_updates_per_second=round(500000 / 10 / 60 / 60))
      self.emu.setup_ws_updates(label='support_task', **freq, get_ws_upd_fn=self.my_upd_support_task, ws_type='sqm')
      self.emu.setup_psinfo(start_after_seconds=20, num_seconds=50)
      self.navigate_to_sqm_tab()
      self.emu.await_updates_completed()

   def my_upd_support_task(self, i):
      #tech = fake.get_tech(1000-i)
      st = self.support_tasks[i%100]
      st.category = random.choice(['YO', 'HEY', 'Hi', 'Hello', 'Dude', 'Howdy', 'Hola'])
      return ['support_task', st.data]

   
      
"""
  specs:
    100,000 support tasks
    10 categories
    50 tasks per ent_prsn_id -> 2000 ent_prsns
    30k techs for 100k tasks
    50% of tasks have wo -> 50k wos
    over 10 hour window 500,000 comms

"""                  
