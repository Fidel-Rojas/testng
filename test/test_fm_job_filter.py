import logging
import pytest
from datetime import date
import code
import base_case
import fsm_tdm

log = logging.getLogger(__name__)

class TC(base_case.BaseCaseFmTab):
         
   def my_job(self, i, job_state=None):
      job_states = ['AS', 'IS', 'CP']
      job_rcs = ['RCG1', 'RCG2', 'RCG2']
      job = self.create_job(i, tech=0)
      job.est = self.to_tz_dt(i%4+8)
      job.job_state = job_state or job_states[i % len(job_states)]
      job.service_area = job_rcs[i % len(job_rcs)]
      job.working_area = job.service_area
      if job.job_state in ['IS', 'CP']:
         hour = i%12+8
         job.start_time = self.to_tz_dt(hour)
         if job.job_state == 'CP':
            job.end_time = self.to_tz_dt(hour, 55)
      return job

   def test_job_filter_all_categories(self):
      self.add_tech(0)
      self.add_job(0, tech=0, event_types=['NM','META'], tags=['VIP'])
      self.add_job(1, tech=0, event_types=['NM','JOWNONWORKSHIFT','NOLATLONG'], tags=['Hot','Colors'])
      self.click_to_flctr3900()

      self.click_filter_container_tab('jobs', 'Status')
      self.find_text("0 of 2 Selected")
      self.assert_content(lambda:self.get_job_filter_dict(), {"":"", 'Assigned':'2'})
      self.click_filter_container_tab('jobs', 'Alerts')
      #self.assert_content(lambda:self.get_job_filter_dict(), {"":"", 'NM':'1', 'VIP':'2'})
      self.click_filter_container_tab('jobs', 'Job Type')
      self.assert_content(lambda:self.get_job_filter_dict(), {"":"", 'COMPONENT-SK':'2'})
      self.click_filter_container_tab('jobs', 'Service Area')
      self.assert_content(lambda:self.get_job_filter_dict(), {"":"", 'GRP-RT':'2'})
      self.click_filter_container_tab('jobs', 'Time Slot')
      self.assert_content(lambda:self.get_job_filter_dict(), {"":"", 'IS: 08-18':'2'})
      self.click_filter_container_tab('jobs', 'Dispatch Status')
      self.assert_content(lambda:self.get_job_filter_dict(), {"":"", 'AS':'2'})
      self.click_filter_container_tab('jobs', 'Skill Groups')
      self.assert_content(lambda:self.get_job_filter_dict(), {"":"", 'SK-desc':'2'})
      self.click_filter_container_tab('jobs', 'Labels')
      self.assert_content(lambda:self.get_job_filter_dict(), {"":"", 'VIP':'1', 'Hot':'1', 'Colors':'1'})

   def test_job_filter_selections_in_multiple_categories(self):
      self.add_tech(0)
      for i in range(3):
         self.add_job(self.my_job(i))
      self.click_to_flctr3900()
      
      self.click_filter_container_tab('jobs', 'Status')
      self.find_text("0 of 3 Selected")
      self.click_checkbox('Complete')
      self.find_text("0 of 1 Selected")
      self.click_checkbox('In Service')
      self.find_text("0 of 2 Selected")
      self.complete_job(0)
      self.ws_upd_job(0)
      self.find_text("0 of 3 Selected")
      self.click_checkbox('In Service')
      self.find_text("0 of 2 Selected")

      self.click_filter_container_tab('jobs', 'Service Area')
      self.click_checkbox('RCG1')
      self.find_text("0 of 1 Selected")


class TestResponsiveToWsUpd(base_case.BaseCaseFmTab):
   def test_job_status_changes(self):
      tech0 = self.add_tech(0)
      job0 = self.add_job(0)
      job1 = self.add_job(1)
      self.click_to_flctr3900()

      self.click_filter_container_tab('jobs', 'Status')

      self.assert_content(lambda:self.get_job_filter_dict(), {'Unassigned': '2'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))
      self.find_text("0 of 2 Selected")

      self.assign_job(job1, tech0)
      self.ws_upd_job(job1)
      self.click_checkbox('Unassigned')
      self.find_text("0 of 1 Selected")
      self.assert_content(lambda:self.get_job_filter_dict(), {'Unassigned': '1', 'Assigned': '1'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0))

      self.click_checkbox('Unassigned')
      self.find_text("0 of 2 Selected")
      self.click_checkbox('Assigned')
      self.find_text("0 of 1 Selected")
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job1))

      self.click_checkbox('Assigned')
      self.complete_job(job1)
      self.ws_upd_job(job1)
      self.click_checkbox('Complete')
      self.find_text("0 of 1 Selected")
      self.assert_content(lambda:self.get_job_filter_dict(), {'Unassigned': '1', 'Complete': '1'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job1))

   def test_job_status_changes(self):
      job0 = self.add_job(0)
      job1 = self.add_job(1, t=None, work_order_type='NC')
      self.click_to_flctr3900()

      self.click_filter_container_tab('jobs', 'Job Type')

      self.assert_content(lambda:self.get_job_filter_dict(), {'NC': '1', 'COMPONENT-SK': '1'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))
      self.find_text("0 of 2 Selected")
      
      job0.work_order_type = 'NC'
      self.ws_upd_job(job0)
      self.click_checkbox('NC')
      self.find_text("0 of 2 Selected")
      self.assert_content(lambda:self.get_job_filter_dict(), {'NC': '2'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))

      self.click_checkbox('NC')
      job0.work_order_type = 'COMPONENT-SK'
      self.ws_upd_job(job0)
      self.click_checkbox('COMPONENT-SK')
      self.find_text("0 of 1 Selected")
      self.assert_content(lambda:self.get_job_filter_dict(), {'NC': '1', 'COMPONENT-SK': '1'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0))

      self.click_checkbox('COMPONENT-SK')
      self.click_checkbox('NC')
      self.find_text("0 of 1 Selected")
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job1))

   def test_service_area_changes(self):
      job0 = self.add_job(0)
      job1 = self.add_job(1, t=None, service_area='GRP-RU')
      self.click_to_flctr3900()

      self.click_filter_container_tab('jobs', 'Service Area')
      self.assert_content(lambda:self.get_job_filter_dict(), {'GRP-RT': '1', 'GRP-RU': '1'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))
      self.find_text("0 of 2 Selected")

      job1.service_area = 'GRP-RT'
      self.ws_upd_job(job1)
      self.click_checkbox('GRP-RT')
      self.find_text("0 of 2 Selected")
      self.assert_content(lambda:self.get_job_filter_dict(), {'GRP-RT': '2'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))

      self.click_checkbox('GRP-RT')
      job0.service_area = 'GRP-RU'
      self.ws_upd_job(job0)
      self.click_checkbox('GRP-RU')
      self.find_text("0 of 1 Selected")
      self.assert_content(lambda:self.get_job_filter_dict(), {'GRP-RT': '1', 'GRP-RU': '1'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0))

      self.click_checkbox('GRP-RU')
      self.click_checkbox('GRP-RT')
      self.find_text("0 of 1 Selected")
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job1))

   def test_timeslot_changes(self):
      idg = fsm_tdm.IdGen(39)
      job0 = self.add_job(0)
      job1 = self.add_job(1, t=None, timeslot_code=idg.get_ts_info(1012).abbr, timeslot_id=idg.timeslot_id(1012))
      time818_code = 'IS: 08-18'
      time1012_code = 'KM: 10-12'
      self.click_to_flctr3900()

      self.click_filter_container_tab('jobs', 'Time Slot')

      self.assert_content(lambda:self.get_job_filter_dict(), {'IS: 08-18': '1', 'KM: 10-12': '1'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))
      self.find_text("0 of 2 Selected")
      job0.timeslot_id = idg.timeslot_id(1012)
      job0.timeslot_code = idg.get_ts_info(1012).abbr
      self.ws_upd_job(job0)

      self.click_checkbox(time1012_code)
      self.find_text("0 of 2 Selected")
      self.assert_content(lambda:self.get_job_filter_dict(), {'KM: 10-12': '2'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))

      self.click_checkbox(time1012_code)
      job1.timeslot_id = idg.timeslot_id(818)
      job1.timeslot_code = idg.get_ts_info(818).abbr
      self.ws_upd_job(job1)
      self.click_checkbox(time818_code)
      self.find_text("0 of 1 Selected")
      self.assert_content(lambda:self.get_job_filter_dict(), {'IS: 08-18': '1', 'KM: 10-12': '1'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job1))

      self.click_checkbox(time818_code)
      self.click_checkbox(time1012_code)
      self.find_text("0 of 1 Selected")
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0))

   def test_dispatch_status_changes(self):
      job0 = self.add_job(0)
      job1 = self.add_job(1, t=None, dispatcher_status='C')
      self.click_to_flctr3900()

      self.click_filter_container_tab('jobs', 'Dispatch Status')

      self.assert_content(lambda:self.get_job_filter_dict(), {'AS': '1', 'CP': '1'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))
      self.find_text("0 of 2 Selected")
      job0.dispatcher_status = 'C'
      self.ws_upd_job(job0)

      self.click_checkbox('CP')
      self.find_text("0 of 2 Selected")
      self.assert_content(lambda:self.get_job_filter_dict(), {'CP': '2'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))

      self.click_checkbox('CP')
      job1.dispatcher_status = 'O'
      self.ws_upd_job(job1)
      self.click_checkbox('AS')
      self.find_text("0 of 1 Selected")
      self.assert_content(lambda:self.get_job_filter_dict(), {'AS': '1', 'CP': '1'})
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job1))

      self.click_checkbox('AS')
      self.click_checkbox('CP')
      self.find_text("0 of 1 Selected")
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0))

      

