import logging
import asyncio
import code
import base_case
from datetime import timedelta
import fake
from fsm_gui_data_packets import Job

log = logging.getLogger(__name__)

class TestBasic(base_case.BaseCaseFmTab):

   def test_jobs_endpoint_data_before_older_ws_upd(self):
      tech0 = self.add_tech(0)
      job0 = self.add_job(0)
      older_job0 = Job(job0.data.copy())
      self.assign_job(job0, tech0)
      older_job0.entity_timestamp = job0.entity_timestamp - timedelta(milliseconds=1)
      job1 = self.create_job(1)
      ws_upd_sent = False
      self.backend_emu.set_response("/jobs", fake.jobs_result([job0, job1]))
      self.click_to_flctr3900()
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))
      self.ws_upd_job(older_job0)
      log.info("ws_upd job %s entity_timestamp %s", older_job0.job_state, older_job0.entity_timestamp)
      log.info("/jobs  job %s entity_timestamp %s", job0.job_state, job0.entity_timestamp)
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))

   def test_ws_upd_data_before_older_jobs_endpoint_data(self):
      tech0 = self.add_tech(0)
      job0 = self.add_job(0)
      older_job0 = Job(job0.data.copy())
      self.assign_job(job0, tech0)
      older_job0.entity_timestamp = job0.entity_timestamp - timedelta(seconds=1)
      job1 = self.create_job(1)
      ws_upd_sent = False
      async def delayed_jobs_result():
         while not ws_upd_sent:
            await asyncio.sleep(0.1)
         log.info('return /jobs endpoint result...')
         return fake.jobs_result([job0, job1])
      self.backend_emu.set_response("/jobs", delayed_jobs_result)
      self.click_to_flctr3900()
      self.backend_emu.wait_for_ws_connect()
      self.ws_upd_job(older_job0)
      ws_upd_sent = True
      log.info("ws_upd job %s entity_timestamp %s", job0.job_state, job0.entity_timestamp)
      log.info("/jobs  job %s entity_timestamp %s", older_job0.job_state, older_job0.entity_timestamp)
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))

   def test_ws_upd_older_data_before_jobs_endpoint_data(self):
      tech0 = self.add_tech(0)
      job0 = self.add_job(0)
      older_job0 = Job(job0.data.copy())
      self.assign_job(job0, tech0)
      older_job0.entity_timestamp = job0.entity_timestamp - timedelta(milliseconds=1)
      job1 = self.create_job(1)
      ws_upd_sent = False
      async def delayed_jobs_result():
         while not ws_upd_sent:
            await asyncio.sleep(0.1)
         log.info('return /jobs endpoint result...')
         return fake.jobs_result([older_job0, job1])
      self.backend_emu.set_response("/jobs", delayed_jobs_result)
      self.click_to_flctr3900()
      self.backend_emu.wait_for_ws_connect()
      self.ws_upd_job(job0)
      ws_upd_sent = True
      log.info("ws_upd job %s entity_timestamp %s", older_job0.job_state, older_job0.entity_timestamp)
      log.info("/jobs  job %s entity_timestamp %s", job0.job_state, job0.entity_timestamp)
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(job0, job1))

