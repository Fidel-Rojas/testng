import logging
import pytest
import seleniumbase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import selenium.common.exceptions
import time
from datetime import datetime
from datetime import date as dt_date
from datetime import time as dt_time
import re
import pytz
import code
from types import SimpleNamespace
from unittest.mock import ANY
import fake
from fsm_gui_data_packets import TechStatus, JobResched
log = logging.getLogger(__name__)

class BaseCase(seleniumbase.BaseCase):
   logged_in = False
   
   @pytest.fixture(autouse=True)
   def grab_fixtures(self, setup, mso_fixture, backend_emu, request):
      self.mso = mso_fixture
      self.backend_emu = backend_emu
      self.emu = backend_emu
      self.pytest_request = request

   def setUp(self):
      super().setUp()
      self.jobs = []
      self.techs = []
      self.support_tasks = []
      self.config = self.get_config()
      if not self.config.reuse_session or not self.logged_in:
         self.login()
         BaseCase.logged_in = True

   def tearDown(self):
      if self.config.code_interact_after_test:
         code.interact(local=locals())
      super().tearDown()
      
   def get_config(self):
      request = self.pytest_request
      return SimpleNamespace(
         code_interact = request.config.getoption('--code-interact'),
         code_interact_after_test = request.config.getoption('--code-interact-after-test'),
         reuse_session = (request.config.getoption('--reuse-session')
                           or request.config.getoption('--rs'))
      )
   
   def utcnow(self):
      return self.backend_emu.utcnow()
   
   def xpath_by_class(self, cls):
      return f".//div[contains(concat(' ',@class,' '),' {cls} ')]"
      
   def find_by_class(self, parent, cls):
      return parent.find_element(By.XPATH, self.xpath_by_class(cls))

   def snarfify(self, rows, columns=None):
      if rows and not columns:
         columns = list(rows[0].keys())
      return dict(columns=columns, rows=[[row.get(f) for f in columns] for row in rows])

   def login(self):
      url = self.backend_emu.url_for(f"/ent/{self.mso}/fsm/login")
      log.info('url %s', url)
      #self.set_window_size(2400, 1200)
      self.set_window_size(1840, 800)
      self.open(url)
      self.type("#username", "J39")
      self.type("#password", "B")
      self.type("#password", Keys.RETURN)

   def navigate_to_tab(self, label):
      self.navigate_to_initial_menu()
      self.click(f".//*[contains(text(), '{label}')]")

   def navigate_to_initial_menu(self):
      if self.is_element_clickable(".//*[contains(@class,'title-label')]"):
         self.click(".//*[contains(@class,'title-label')]", timeout=1)
      
   def assert_content(self, fn, exp, timeout=10):
      start = time.time()
      while time.time() < start + timeout:
         act = None
         try:
            act = fn()
         except selenium.common.exceptions.StaleElementReferenceException:
            pass
         except selenium.common.exceptions.NoSuchElementException:
            if self.config.code_interact:
               print(f'caught NoSuchElementException:')
               code.interact(local=locals())
            else:
               raise
         if act == exp:
            break
         time.sleep(0.1)
      if self.config.code_interact and act != exp:
         print(f"assertion failed:\n  ACTUAL: {act}\n  EXPECT: {exp}")
         code.interact(local=locals())
      assert act == exp
      
   def ws_upd_tech_status(self, tech, tech_status_cd):
      tech.tech_status_cd = tech_status_cd
      tech.tech_status_datetime = self.backend_emu.utcnow().isoformat()
      self.backend_emu.send_ws_update(['tech_status', TechStatus.from_obj(tech).data])
      
   def ws_upd_tech_shift_skill_codes(self, tech, skill_codes):
      self.first_tsh(tech).skill_codes = skill_codes
      tech.entity_datetime = self.backend_emu.utcnow().isoformat()
      self.backend_emu.send_ws_update(['tech', tech.data])

   def ws_upd_job(self, job):
      job = self.to_job(job)
      self.backend_emu.send_ws_update(['job', job.data])

   def ws_upd_support_task(self, st):
      st = self.to_support_task(st)
      self.backend_emu.send_ws_update(['support_task', st.data], ws_type='sqm')

   def assign_job(self, job, tech, est=None, duration_mins=55):
      job = self.to_job(job)
      if tech is None:
         job.job_state = 'UA'
         job.tech_id = None
         job.tech_num = None
         job.tech_first_name = None
         job.tech_last_name = None
         job.est = None
         job.assignment_type = 'U'
         job.duration_mins = None
      else:
         job.job_state = 'AS'
         job.tech_id = tech.tech_id
         job.tech_num = tech.tech_num
         job.tech_first_name = tech.tech_first_name
         job.tech_last_name = tech.tech_last_name
         job.est = self.to_tz_dt(est or dt_time(10,15))
         job.assignment_type = 'M'
         job.duration_mins = duration_mins

   def in_service_job(self, job, start_time=None):
      job = self.to_job(job)
      job.job_state = 'IS'
      job.start_time = self.to_tz_dt(start_time or dt_time(8))
      
   def complete_job(self, job, start_time=None, end_time=None):
      job = self.to_job(job)
      job.job_state = 'CP'
      job.start_time = self.to_tz_dt(start_time or job.start_time or dt_time(8))
      job.end_time = self.to_tz_dt(end_time or dt_time(8,55))
      
   def to_support_task(self, st):
      return self.support_tasks[st] if isinstance(st, int) else st
         
   def to_job(self, j):
      return self.jobs[j] if isinstance(j, int) else j
         
   def to_tech(self, t):
      return self.techs[t] if isinstance(t, int) else t
         
   def get_user_id(self):
      return 39099999
   
   def bu_today(self):
      return self.to_tz_dt().date().isoformat()

   def to_tz_dt(self, val=dt_time(), *args):
      if isinstance(val, int):
         val = dt_time(val, *args)
      if isinstance(val, dt_time):
         today = dt_date.today()
         tz = pytz.timezone('America/New_York')
         dt = tz.localize(datetime.combine(today, val))
      else:
         dt = val
      return dt

   def create_job(self, data, tech=None, **kwargs):
      if isinstance(data, int):
         job = fake.get_job(data, **kwargs)
         if tech is not None:
            tech = self.to_tech(tech)
            self.assign_job(job, tech, est=kwargs.get('est'))
      else:
         job = data
      return job
            
   def add_job(self, data, **kwargs):
      job = self.create_job(data, **kwargs)
      self.jobs.append(job)
      self.backend_emu.set_response("/jobs", fake.jobs_result(self.jobs))
      return job
            
   def create_tech(self, data, **kwargs):
      if isinstance(data, int):
         tech = fake.get_tech(data, **kwargs)
      else:
         tech = data
      return tech

   def add_tech(self, data, **kwargs):
      tech = self.create_tech(data, **kwargs)
      self.techs.append(tech)
      self.backend_emu.set_response("/techs", fake.techs_result(self.techs))
      return tech

   def create_support_task(self, data, **kwargs):
      if isinstance(data, int):
         st = fake.get_support_task(data, **kwargs)
      else:
         st = data
      return st
            
   def add_support_task(self, data, **kwargs):
      st = self.create_support_task(data, **kwargs)
      self.support_tasks.append(st)
      self.backend_emu.set_response("/support-tasks", lambda:fake.support_tasks_result(self.get_user_support_tasks()))
      return st

   def get_user_support_tasks(self):
      return filter(lambda st: st.ent_prsn_id is None or st.ent_prsn_id == self.get_user_id(), self.support_tasks)
   
   def get_element_if_present(self, parent_el, by, xpath):
      el = None
      try:
         el = parent_el.find_element(by, xpath)
      except selenium.common.exceptions.NoSuchElementException:
         pass
      return el

   def get_element_text_if_present(self, parent_el, by, xpath):
      el = self.get_element_if_present(parent_el, by, xpath)
      return el.text if el else None
   
class BaseCaseFmTab(BaseCase):
   in_fm_tab = False
   
   def setUp(self):
      super().setUp()
      self.navigate_to_tab('Fulfillment')

   def clear_all(self):
      self.click('.//*[contains(text(),"Clear All")]')
      self.job_list_organize('Sort', 'Job Number')
      self.job_list_organize('Group', 'Ungroup')
      self.tech_list_organize('Sort', 'Tech Number')
      self.tech_list_organize('Group', 'Ungroup')
      self.search_job("")
      self.search_tech("")
      
   def expand_tree_element(self, title):
      self.get(f'div[title="{title}"]').find_element(By.XPATH, "../ancestor::tr//button").click()
   def better_expand_tree_element(self, title):
      self.click(f'p-treetabletoggler[name="{title}"] .ui-treetable-toggler')
   def get_tree_element(self, title):
      return self.get(f'div[title="{title}"]').find_element(By.XPATH, "..").find_element(By.CLASS_NAME, "p-checkbox")
   def click_tree_element(self, title):
      self.get_tree_element(title).click()
   def better_click_tree_element(self, title):
      self.click(f'p-treetablecheckbox[name="{title}"] .p-checkbox', timeout=1)
         
   def click_to_flctr3900(self):
      self.clear_all()
      self.expand_tree_element('dvsn3920')
      self.expand_tree_element('site3910')
      self.click_tree_element('flctr3900')

   def click_checkbox(self, title):
      xpath = f'.//div[@title="{title}"]'
      self.scroll_into_view(xpath, by="xpath", timeout=None)
      self.find_element(xpath).find_element(By.XPATH, "..").find_element(By.CLASS_NAME, "checkbox-small").click()
      
   def click_filter_container_tab(self, filter_container_class, text):
      self.find_element(f".//div[contains(@class,'{filter_container_class}')]").find_element(By.XPATH, f".//*[contains(text(),'{text}')]//parent::*").click()
      
   def get_tech_filter_dict(self):
      return self.get_filter_container_dict('techs')
   
   def get_job_filter_dict(self):
      return self.get_filter_container_dict('jobs')
   
   def get_filter_container_dict(self, filter_container_class):
      h = {}
      for name_el, count_el in zip(self.find_elements(f".//div[contains(@class,'{filter_container_class}')]//div[@class='bar-row-name']"),
                                   self.find_elements(f".//div[contains(@class,'{filter_container_class}')]//div[@class='bar-row-count']")):
         h[name_el.text] = count_el.text
      return h
  
   def get_techs_statuses(self):
      return {span.text:div.text for span, div in zip(self.find_elements(".//technician-tile//span[contains(@class,'tech-number')]"),
                                                      self.find_elements(".//technician-tile//div[contains(@class,'status')]"))}
   def get_techs_jobs_from_gantt(self):
      h = {}
      for span, div in zip(self.find_elements(".//technician-tile//span[@class='tech-number']"),
                           self.find_elements(".//day-row-tile")):
         jdivs = div.find_elements(By.XPATH, ".//div[contains(@class,'job-number')]")
         h[span.text] = [jdiv.text for jdiv in jdivs]
      return h
  
   def get_tech_list_counts(self):
      h = {}
      els = self.find_element(".//div[@id='techs-list']").find_elements(By.XPATH, ".//div[contains(@class,'list-group')]")
      for el in els:
         name = el.find_element(By.XPATH, ".//div[contains(@class,'name')]").text
         value = el.find_element(By.XPATH, ".//div[contains(@class,'counts')]").text
         h[name] = value
      return h
  
   def get_tech_list_techs(self):
      objs = []
      for div in self.find_elements(".//technician-tile"):
         objs.append(dict(
            tech_num = div.find_element(By.XPATH, ".//*[contains(@class,'tech-number')]").text,
            tech_name = div.find_element(By.XPATH, ".//*[contains(@class,'tech-name')]").text,
            tech_status = div.find_element(By.XPATH, ".//*[contains(@class,'status')]").text,
         ))
      return objs
  
   def get_job_list_jobs(self):
      objs = []
      for div in self.find_elements(".//job-list-tile"):
         objs.append(dict(
            work_order_number=div.get_attribute('id'),
            job_number=div.find_element(By.XPATH, ".//div[@class='job-number']").text,
            job_state=div.find_element(By.XPATH, ".//div[@class='status']").text,
            skill_code_0=div.find_element(By.XPATH, ".//div[contains(@class,'letter-item')]").text,
            points=div.find_element(By.XPATH, ".//div[contains(@class,'point')]").text,
            tech_num=div.find_element(By.XPATH, ".//div[contains(@class,'tech-num')]").text,
            tech_name=div.find_element(By.XPATH, ".//div[contains(@class,'tech-name')]").text,
            cust_name=div.find_element(By.XPATH, ".//div[contains(@class,'name')]").text,
            street=div.find_element(By.XPATH, ".//div[contains(@class,'street')]").text,
            city=div.find_element(By.XPATH, ".//div[contains(@class,'city')]").text,
            date=datetime.strptime(div.find_element(By.XPATH, ".//div[contains(concat(' ',@class,' '),' date ')]").text, "%m.%d.%Y").date(),
            hours=div.find_element(By.XPATH, ".//div[contains(@class,'hours')]").text,
            est=self.get_element_text_if_present(div, By.XPATH, ".//div[contains(@class,'est')]"),
            tags=[el.text for el in div.find_elements(By.XPATH, ".//div[contains(concat(' ',@class,' '),' tag ')]")],
         ))
      return objs
  
   def job_list_exp(self, *jobs):
      exp = []
      date = self.get_fm_tab_date()
      for job in jobs:
         job = self.to_job(job)
         exp.append(dict(
            work_order_number = job.work_order_number,
            job_number = job.job_number,
            job_state = job.job_state,
            skill_code_0 = job.skill_codes[0],
            est = datetime.strftime(job.est, "EST %H:%M %p") if job.job_state == 'AS' else None,
            points = f"{job.estimated_points} PTS",
            tech_num = job.tech_num or '',
            tech_name = f"{job.tech_first_name} {job.tech_last_name}" if job.tech_num else '',
            cust_name = job.cust_name,
            street = " ".join([v for v in (job.street_name, job.street_name_2, job.building, job.apartment) if v is not None]),
            city = " ".join([v for v in (job.city, job.state, job.zip_code_plus) if v is not None]),
            date = date, # no date in job, why scrape from screen?
            hours = (fake.get_timeslot(job.timeslot_id))['timeslot_desc'],
            tags = job.tags
         ))
      return exp

   def get_fm_tab_date(self):
      month_day, year = [el.text for el in self.find_element('.//*[@data-testid="viewing-text"]', timeout=1).find_elements(By.CSS_SELECTOR, "span")]
      month_name, day = re.findall(r'\w+', month_day)
      month_date = datetime.strptime(month_name, "%B").date()
      return dt_date(int(year), month_date.month, int(day))

   def tech_list_organize(self, group_or_sort, key):
      self.click(f".//techs-list//div[@title='{group_or_sort} By']")
      options_el = self.find_element(f".//wfx-select-item-list//div[contains(text(),'{group_or_sort} Techs By')]//..")
      option_els = options_el.find_elements(By.XPATH, self.xpath_by_class('item-text-field'))
      option_el = {e.text:e for e in option_els}
      option_el[key].click()
      
   def job_list_organize(self, group_or_sort, key):
      self.click(f".//jobs-list//div[@title='{group_or_sort} By']")
      options_el = self.find_element(f".//wfx-select-item-list//div[contains(text(),'{group_or_sort} Jobs By')]//..")
      option_els = options_el.find_elements(By.XPATH, self.xpath_by_class('item-text-field'))
      option_el = {e.text:e for e in option_els}
      option_el[key].click()

   def get_job_list_counts(self):
      h = {}
      els = self.find_element(".//jobs-list").find_elements(By.XPATH, ".//div[contains(@class,'list-group')]")
      for el in els:
         name = el.find_element(By.XPATH, ".//div[contains(@class,'name')]").text
         value = el.find_element(By.XPATH, ".//div[contains(@class,'counts')]").text
         h[name] = value
      return h
      
   def search_tech(self, text):
      self.type(".//input[@placeholder='Search Technicians']", text)
      
   def search_job(self, text):
      self.type(".//input[@placeholder='Search Jobs']", text)
      
   def first_tsh(self, tech):
      return (tech.get_tshs())[0]
  
   def tech_list_exp(self, *techs):
      exp = []
      for tech in techs:
         exp.append({
            'tech_num': tech.tech_num,
            'tech_name': f"{tech.tech_first_name} {tech.tech_last_name}",
            'tech_status': tech.tech_status_cd,
         })
      return exp
  
class BaseCaseSqmTab(BaseCaseFmTab):
   
   def setUp(self):
      super().setUp()

   def navigate_to_sqm(self):
      self.navigate_to_fm_flctr3900()
      self.navigate_to_sqm_tab()
      
   def navigate_to_fm_flctr3900(self):
      self.click_to_flctr3900()
      self.navigate_to_initial_menu()

   def navigate_to_sqm_tab(self):
      self.navigate_to_tab('Support Queue Management')

   def click_sidebar_support_tasks_button(self):
      self.find_element(f".//wfx-nav-button[contains(@class,'support-tasks')]").click()

   def click_sidebar_filter_button(self):
      self.find_element(f".//wfx-nav-button[contains(@class,'areas-of-focus')]").click()

   def click_sidebar_accept_task(self):
      self.click(f".//*[contains(text(),'Accept Task')]")
      
   def click_sqm_accept_task(self):
      self.find_element(f".//wfx-button[contains(@class,'accept-task')]/..").click()

   def get_sqm_support_tasks(self):
      objs = []
      container = self.get(".//task-list-container")
      for div in container.find_elements(By.XPATH, ".//task-tile"):
         objs.append(dict(
            task_id=self.find_by_class(div, 'task-id').text,
            task_category=self.find_by_class(div, 'task-category').text,
            subject=self.find_by_class(div, 'subject').text,
            letters_sub=self.find_by_class(div, 'letters-sub').text,
            status=self.find_by_class(div, 'working-status').text,
            working_time=self.find_by_class(div, 'working-time').text,
            agent_info=self.get_element_text_if_present(div, By.XPATH, self.xpath_by_class('agent-info')),
         ))
      return objs
  
   def get_sidebar_support_task_list(self):
      container = self.get(".//div[contains(@class,'my-support-tasks-container')]")
      info = {}
      for div in container.find_elements(By.XPATH, ".//div[contains(@class,'value-box')]"):
         title = div.find_element(By.XPATH, ".//div[contains(@class,'title')]").text
         value = div.find_element(By.XPATH, ".//div[contains(@class,'value')]").text
         info[title] = value
      info['support_tasks'] = self.get_sidebar_support_tasks()
      return info

   def get_sidebar_support_tasks(self):
      objs = []
      for div in self.find_elements(".//task-tile"):
         objs.append(dict(
            task_id=self.find_by_class(div, 'task-id').text,
            task_category=self.find_by_class(div, 'task-category').text,
            subject=self.find_by_class(div, 'subject').text,
            letters_sub=self.find_by_class(div, 'letters-sub').text,
            status=self.find_by_class(div, 'working-status').text,
            working_time=self.find_by_class(div, 'working-time').text,
            agent_info=self.find_by_class(div, 'agent-info').text,
         ))
      return objs

   def get_sqm_support_task_details_container(self):
      parent = self.get(self.xpath_by_class('details-container'))
      info = dict(
         status=self.find_by_class(parent, 'status').text,
         time=self.find_by_class(parent, 'time').text,
         task_id=self.find_by_class(parent, 'title').text,
         category=self.find_by_class(parent, 'light').text,
         task_defn_name=self.find_by_class(parent, 'main-text').text,
         create_dt=self.find_by_class(parent, 'main-text').text,
         assigned_initials=self.find_by_class(parent, 'initials').text,
         assigned_name=self.find_by_class(parent, 'name').text,
         associated_initials=self.find_by_class(parent, 'initials').text,
         associated_name=self.find_by_class(parent, 'name').text,
      )
      msgs = []
      badge = None
      cc_div = None
      try:
         cc_div = self.get(self.xpath_by_class('content-container'))
      except:
         pass
      if cc_div:
         for bubble in cc_div.find_elements(By.CSS_SELECTOR, 'message-bubble'):
            msgs.append(dict(
               sender=self.find_by_class(bubble, 'sender').text,
               time=self.find_by_class(bubble, 'time').text,
               message=self.find_by_class(bubble, 'speech-bubble').text,
            ))
            badge = 1 if self.find_by_class(cc_div, 'badge') else 0
      info['msg_info'] = dict(messages=msgs, badge=badge)
      return info
   
   def get_sidebar_support_task_selected_container(self):
      parent = self.get(".//div[contains(@class,'task-selected-container')]")
      info = dict(
         status=self.find_by_class(parent, 'status').text,
         time=self.find_by_class(parent, 'time').text,
         task_id=self.find_by_class(parent, 'code').text,
         category=self.find_by_class(parent, 'description').text,
         task_defn_name=self.find_by_class(parent, 'type').text,
         create_dt=self.find_by_class(parent, 'created-value').text,
         assigned_initials=self.find_by_class(parent, 'initials').text,
         assigned_name=self.find_by_class(parent, 'name').text,
         associated_initials=self.find_by_class(parent, 'initials').text,
         associated_status=self.find_by_class(parent, 'status').text,
         associated_name=self.find_by_class(parent, 'name').text,
      )
      return info

   def click_sidebar_all_tasks(self):
      self.click(self.xpath_by_class('all-tasks-icon'))

   def click_sidebar_support_task_item_blue_arrow(self):
      task_item = self.get(self.xpath_by_class('task-item'))
      self.find_by_class(task_item, 'icon').click()
      
   def click_sqm_filter_tab(self, text):
      self.click(f".//wfx-tab-view//*[contains(text(),'{text}')]//parent::*")

   def click_sqm_filter_tab_entry(self, text):
      self.click(f".//wfx-tab-view//div[contains(text(),'{text}')]//parent::*//wfx-checkbox")

   def sidebar_support_tasks_exp(self, *sts):
      exp = []
      for st in sts:
         st = self.to_support_task(st)
         exp.append(dict(
            task_id = str(st.st_id),
            task_category = st.category,
            subject = st.subject,
            letters_sub = ("".join([s[0] for s in st.category.split()]) + st.category[1])[0:2],
            status = st.status,
            working_time = ANY,
            agent_info = 'David Totschnig' if st.ent_prsn_id else None,
         ))
      return exp
   
   def sqm_support_tasks_exp(self, *sts):
      exp = []
      for st in sts:
         st = self.to_support_task(st)
         exp.append(dict(
            task_id = str(st.st_id),
            task_category = st.category,
            subject = st.subject,
            letters_sub = st.category[0:2],
            status = st.status,
            working_time = ANY,
            agent_info = 'David Totschnig' if st.ent_prsn_id else None,
         ))
      return exp

   def click_sqm_sort_by(self, text):
      self.click(".//div[contains(@class,'sortBy-icon')]")
      self.click(f".//wfx-popover//div[contains(@class,'isOpen')]//div[contains(text(),'{text}')]")

   def click_sqm_group_by(self, text):
      self.click(".//div[@title='Group By']")
      self.click(f".//wfx-popover//div[contains(@class,'isOpen')]//div[contains(text(),'{text}')]")

   def get_sqm_group_counts(self):
      h = {}
      els = self.find_element(self.xpath_by_class('grouped-list')).find_elements(By.XPATH, self.xpath_by_class('list-group'))
      for el in els:
         name = el.find_element(By.XPATH, ".//div[contains(@class,'name')]").text
         value = el.find_element(By.XPATH, ".//div[contains(@class,'counts')]").text
         h[name] = value
      return h
  
      
