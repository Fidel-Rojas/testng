import logging
import code
import base_case
import fake
log = logging.getLogger(__name__)

class TestBasic(base_case.BaseCaseFmTab):

   def test_job_tile(self):
      bu0tech = self.create_tech(0)
      bu0job = self.create_job(0) 
      bu1tech = self.create_tech(1, tsh=dict(current_fulfill_center_bu_id=3901))
      bu1job = self.create_job(1, bu_id=3901)
      today = self.bu_today()
      user_id = self.get_user_id()
      self.backend_emu.set_response(f"3900/date/{today}/user/{user_id}/jobs", fake.jobs_result([bu0job]))
      self.backend_emu.set_response(f"3901/date/{today}/user/{user_id}/jobs", fake.jobs_result([bu1job]))
      self.backend_emu.set_response(f"3900/date/{today}/user/{user_id}/techs", fake.techs_result([bu0tech]))
      self.backend_emu.set_response(f"3901/date/{today}/user/{user_id}/techs", fake.techs_result([bu1tech]))
      self.click_to_flctr3900()
      self.click_tree_element('flctr3901')
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(bu0job, bu1job))
      self.assert_content(lambda:self.get_tech_list_techs(), self.tech_list_exp(bu0tech, bu1tech))
      self.click_tree_element('flctr3901')
      self.assert_content(lambda:self.get_job_list_jobs(), self.job_list_exp(bu0job))
      self.assert_content(lambda:self.get_tech_list_techs(), self.tech_list_exp(bu0tech))
      # assert self.is_element_clickable(self.get_tree_element('flctr3901'))
      # code.interact(local=locals())
