# MyApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

### Steps for local WSL2 setup to run the kktest
1. Install WSL2 from windows cmd
    wsl --install 
   or if you already installed then update it from windows cmd
    wsl --update
2. Install ubuntu 
    wsl --install -d Ubuntu-22.04
3. Switch to ubuntu terminal using command - 
    wsl -d Ubuntu-22.04
4. Install all the depandacies to run the test cases
    sudo apt-get update
    sudo apt-get install curl
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash
    nvm install --lts
    nvm install 12.21.0
    sudo apt-get -y install python3-pip
    sudo apt-get install apache2  
    sudo apt-get install subversion
    sudo apt-get install git
    sudo apt-get install make
5. Follow this link to install latest chrome and chrome driver -https://www.gregbrisebois.com/posts/chromedriver-in-wsl2/
6. Run the command - export DISPLAY=:0
7. Checkout kodename kairos under /home/<user_dir> 
    git clone https://bitbucket.csgi.com/scm/wfx/kodename-kairos.git
8. Run the following commands to run the test cases
    cd kodename-kairos
    npm set registry http://wfxcmdev07.csgicorp.com:4873
    npm i
    npm run build:watch
    npm run install:test
    npm run test:all